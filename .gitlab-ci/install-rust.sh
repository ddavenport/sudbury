#! /bin/bash

set -e -o xtrace -o errexit
export CARGO_HOME='/usr/local/cargo'

RUSTUP_VERSION=1.25.1
RUST_VERSION=$1
RUST_ARCH="x86_64-unknown-linux-gnu"

RUSTUP_URL=https://static.rust-lang.org/rustup/archive/$RUSTUP_VERSION/$RUST_ARCH/rustup-init
curl -O $RUSTUP_URL

chmod +x rustup-init;
./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION;
rm rustup-init;
chmod -R a+w $RUSTUP_HOME $CARGO_HOME

source "/usr/local/cargo/env"

rustup --version
cargo --version
rustc --version

rustup component add rustfmt
rustup component add clippy

cargo install --force cargo-deny
cargo install --force cargo-tarpaulin

if [ "$RUST_VERSION" = "nightly" ]; then
  rustup component add rustfmt --toolchain nightly

  # Documentation tools
  cargo install --force rustdoc-stripper
fi
