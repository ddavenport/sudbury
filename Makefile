# Example client run:
# WAYLAND_DEBUG=1  WESTON_DATA_DIR=/home/bwidawsk/dev/comp/weston/data WAYLAND_DISPLAY=wayland-2 ./weston-simple-egl &
#
# Example to generate a flame graph on the client
# `WESTON_DATA_DIR=/home/bwidawsk/dev/comp/weston/data WAYLAND_DISPLAY=wayland-1 perf record --call-graph dwarf ./weston-simple-egl`
# `perf script | stackcollapse-perf.pl |flamegraph.pl > perf.svg`
#
# Example to generate a flame graph on the server
# `sudo -E SMITHAY_DIRECT_TTY=/dev/tty1 cargo flamegraph -- -d card0`

DUT_IP ?= saberhagen
DUT_PORT ?= 22
TTY ?= tty1
SMITHAY_TTY = SMITHAY_DIRECT_TTY=/dev/${TTY}
TARGET ?= x86_64-unknown-linux-gnu
NORMAL_RUN = sudo -E ${SMITHAY_TTY} ./sudbury -d card0 --fps
DEBUG_RUN = RUST_BACKTRACE=1 RUST_LOG=debug ${NORMAL_RUN}

build-debug:
	cargo build

build-release:
	cargo build --release

build-memsan:
	RUSTFLAGS='-Zsanitizer=memory -Zsanitizer-memory-track-origins' cargo build -Zbuild-std --target ${TARGET}

build-asan: FORCE
	RUSTFLAGS=-Zsanitizer=address cargo build -Zbuild-std --target ${TARGET}

gdb-local: debug-build
	cargo with "rust-gdb --args {bin} {args}" -- run --bin sudbury --

lldb-local: debug-build
	cargo with "rust-lldb -- {bin} {args}" -- run --bin sudbury --

xfer-release-build: FORCE
	rsync --checksum -e "ssh -p ${DUT_PORT}" target/release/sudbury ${DUT_IP}:

xfer-debug-build: FORCE
	rsync --checksum -e "ssh -p ${DUT_PORT}" target/debug/sudbury ${DUT_IP}:

xfer-san-build: FORCE
	rsync --checksum -e "ssh -p ${DUT_PORT}" target/${TARGET}/debug/sudbury ${DUT_IP}:

run-local: build-release xfer-release-build
	${NORMAL_RUN}

run-local-debug: build-debug xfer-debug-build
	${DEBUG_RUN}

run: build-release xfer-release-build
	ssh -t -p ${DUT_PORT} ${DUT_IP} ${NORMAL_RUN}

run-debug: build-debug xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} ${DEBUG_RUN}

# Example: debug input: `make run-Xdebug XDEBUG=sudbury::input`
run-Xdebug: build-debug xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} RUST_LOG=${XDEBUG} ${NORMAL_RUN}

run-heaptrack: build-release xfer-release-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} sudo -E "${SMITHAY_TTY}" heaptrack ./sudbury -d card0

run-trace: build-debug xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} RUST_LOG=trace ${DEBUG_RUN} -S sudbury.log

run-memsan: build-memsan xfer-san-build run-debug

run-asan: build-asan xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} RUST_LOG=debug sudo -E ASAN_OPTIONS=detect_leaks=1,detect_stack_use_after_return=1 ${SMITHAY_TTY} ./sudbury -d card0

run-malloc-debug: build-debug xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} MALLOC_CHECK_=3 RUST_LOG=debug sudo -E ${SMITHAY_TTY} ./sudbury -d card0

FORCE:
