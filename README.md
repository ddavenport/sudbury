# Sudbury

A Rust compositor built on top of [Smithay](https://smithay.github.io/) designed
for ChromeOS.

The name is from a town in Massachusetts. It is west of Wayland (and _wester_ of
Weston).

## Anti-Goals

1. __NOT__: Sudbury is a general purpose desktop compositor.

Sudbury can ultimately remove much general purpose functionality that many
expect. Some such things may be convenient to keep for development and debug and
so where possible it can be kept behind a feature flag. Internal abstractions to
support such feature will be squinted at _real hard_. Some features that would
normally appear but do not here:
- [XWayland](https://wayland.freedesktop.org/xserver.html) support
- systemd/seatd integration
- Alternate layout support such as tiling.

Currently libinput support is desirable, but this may be dropped longer term as
well.

2. __NOT__: Sudbury has a lot of rich value-add features that other compositors do
   not have.

Sudbury shall maintain the minimal set of functionality needed which cannot be
added to Smithay directly, for good reason.

3. __NOT__: Sudbury will build a rich ecosystem of its own styles and idioms.

Sudbury leverages Rust and Smithay idioms.

## Details

### Composition

Compositors have an implicit policy decision around when they composite and when
they give the signal to clients to resume their render loop. Some timing
diagrams are included in the repository
![here](resources/damage_with_render.svg) and
![here](resources/damage_no_render.svg). A majority of the description for how
this works can be found in the Rustdoc within the project.

### Environment Variables / Debugging

| NAME Aligned           | Type | Valid Values Aligned | Description                                                   |
| :---                   | :--- | :---                 | :---                                                          |
| SBRY_DAMAGE_AGE        | u8   | 0 - 255              | Override age of the next scanout buffer. (Default actual age) |
| SBRY_RENDER_DIVIDE     | f32  | 0.01 - 0.99          | Split between client and compositor rendering. (Default 0.75) |
| SMITHAY_DIRECT_TTY_ENV | str  | /dev/ttyX            | Override tty terminal to takeover                             |

When a plane is directly scanned out by hardware (either primary or overlay) no
copy happens from the backbuffer to the composited frame. On the other hand,
when a copy happens, a performance penalty is incurred for the case where direct
scan out could have occurred. To visualize when the copy happens, a tint is
applied as part of the shader for debugging. This can be enabled by the key
combination: `mod4 + shift + t`

## TODO

### Popups

Smithay handles most of this already. Just needs a bit of typing.

### Input

While the ultimate input implementation requires more design and effort to
figure out how best to incorporate the existing Chrome OS input experience,
libinput can _and should_ be integrated meanwhile.

Input work has been started. It's merged into main but needs a lot of testing
and improvements.

### Explicit Sync

Instead of the compositor blocking while a client might be rendering slow frames, use explcit sync to make a better determination of whether or not to composit.

https://wayland.app/protocols/linux-explicit-synchronization-unstable-v1

### Aura Protocol

### Chrome Session Management

### Debug Features

1. Damage visualization
2. Better tracing information (different subscriber)

# Build

If trying to run from a TTY, the following should be sufficient:
Build:

`cargo build --release --no-default-features`

Run (on TTY1 and card0)

`sudo -E SMITHAY_DIRECT_TTY=/dev/tty1 ./sudbury -d card0`

Same as above with debug messages

`RUST_LOG=debug sudo -E SMITHAY_DIRECT_TTY=/dev/tty1 ./sudbury -d card0`

## ChromeOS deployment

### Prerequisit

There are a couple steps required to get the environment set up for building sudbury for DUT.

`mkdir third_party/chromiumos-overlay/chromeos-base/sudbury`

`cd third_party/chromiumos-overlay/chromeos-base/sudbury`

`curl -O https://gitlab.freedesktop.org/bwidawsk/sudbury/-/raw/cros/sudbury-9999.ebuild`

`cd ../../../..`

`mkdir third_party/rust_crates/projects/platform/sudbury`

And go through the revendoring steps below.

In order to support local change deployment, the current ebuild is expecting sources to be under platform, so a clone of gitlab repo needs to be added there.

`mkdir platform/sudbury`

`git clone https://gitlab.freedesktop.org/bwidawsk/sudbury.git`

Note that it is possible to modify the ebuild to get the sources directly from git and avoid the local changes, see the following crrev fo example of how this would be done:

`https://crrev.com/c/4148079`


### Updating dependencies/revendoring

If you changed some dependencies, rust_crates will need to be updated for emerge to work.

`cd third_party/rust_crates/projects/platform/sudbury/`

either update Cargo.toml from gitlab

`curl -O https://gitlab.freedesktop.org/bwidawsk/sudbury/-/raw/cros/Cargo.toml`

or copy from local changes

`cp ../../../../../../platform/sudbury/Cargo.toml Cargo.toml`


`cd ../../`

`./populate-workspace.py`

`cd ..`

`python vendor.py -s drm-ffi -s drm-fourcc -s drm-sys -s gbm-sys -s input-sys -s wayland-backend -s wayland-protocols-misc -s wayland-protocols-wlr`

the last step may end up complaining about unrelated issues (CRAB or cargo-vet for instance), this does not affect sudury deployment flow.

Lastly e,erge all the third party crates

`emerge-$BOARD third-party-crates-src`

### Regular deployment flow

Once depedencies have been updated a regular emerge & cros deploy should work.

`emerge-${BOARD} sudbury`

`cros deploy $DUT sudbury`

### Running chrome on top

Patch chrome with native display delegate:

`https://crrev.com/c/3272705`

Enable ozone/wayland backend in the gn args for your build:

`target_os = ["chromeos"]`

`ozone_platform_wayland = "true"`

`ozone_platform = "wayland"`

`use_wayland_egl = "false"`

`use_wayland_gbm = "true"`

Build:

`ninja -C out_$BOARD/Relase chrome`

Deploy:

`deploy_chrome --build-dir=out_$BOARD/Release --device=$DUT --nostartui`

Start sudbury:

`export XDG_RUNTIME_DIR=/run/chrome/`

`sudo sudbury -s smithay.log -d card0`

On the DUT set WAYLAND_DISPLAY to the socket sudbury is:

`export WAYLAND_DISPLAY=wayland-1`

Launch ash chrome:

`restart ui`
