use smithay::{
    delegate_dmabuf,
    wayland::dmabuf::{DmabufHandler, DmabufState, ImportError},
};
use tracing::error;

use crate::Sudbury;

impl DmabufHandler for Sudbury {
    fn dmabuf_state(&mut self) -> &mut DmabufState {
        self.drm_kms.dmabuf_state_mut()
    }

    fn dmabuf_imported(
        &mut self,
        _global: &smithay::wayland::dmabuf::DmabufGlobal,
        dmabuf: smithay::backend::allocator::dmabuf::Dmabuf,
    ) -> Result<(), smithay::wayland::dmabuf::ImportError> {
        if let Err(e) = self.drm_kms.import_dmabuf(&dmabuf) {
            error!("{:?}", e);
            return Err(ImportError::Failed);
        }

        Ok(())
    }
}
delegate_dmabuf!(Sudbury);
