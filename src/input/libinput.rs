use std::convert;

use anyhow::Context;
use smithay::{
    backend::{
        self,
        input::{
            Device, Event, InputEvent, KeyboardKeyEvent, PointerAxisEvent, PointerButtonEvent,
            PointerMotionEvent,
        },
        libinput::{LibinputInputBackend, LibinputSessionInterface},
        session::Session,
    },
    input::pointer::{ButtonEvent, MotionEvent, RelativeMotionEvent},
    reexports::{
        calloop::LoopHandle,
        input::{event::pointer, Libinput},
    },
    utils::SERIAL_COUNTER,
};
use tracing::{debug, error};
use wayland_server::protocol::wl_pointer;

use crate::{input::SudburyInput, GlobalState};

fn input_event(event: InputEvent<LibinputInputBackend>, _meta: &mut (), data: &mut GlobalState) {
    let serial = SERIAL_COUNTER.next_serial();
    match event {
        InputEvent::Keyboard { event } => {
            let Some(keyboard) = data.sudbury.seat.get_keyboard() else { error!("No keyboard"); return; };
            let (code, state) = (event.key_code(), event.state());

            keyboard.input::<(), _>(
                &mut data.sudbury,
                code,
                state,
                serial,
                Event::time_msec(&event),
                move |sudbury, modifiers, keyhand| {
                    super::handle_keyboard(sudbury, state, modifiers, keyhand)
                },
            );
        }
        InputEvent::DeviceAdded { device } => {
            debug!(
                "Input device added {:?}",
                device.syspath().unwrap_or_default()
            );
        }
        InputEvent::DeviceRemoved { device: _ } => todo!(),
        InputEvent::PointerMotion { event } => {
            let new_point = SudburyInput::pointer_motion(&mut data.sudbury, event.delta());
            if let Some(pointer) = data.sudbury.seat.get_pointer() {
                let evt = MotionEvent {
                    location: new_point.0,
                    serial,
                    time: event.time_msec(),
                };
                pointer.motion(&mut data.sudbury, new_point.1.clone(), &evt);
                let evt = RelativeMotionEvent {
                    delta: event.delta(),
                    delta_unaccel: event.delta_unaccel(),
                    utime: event.time(),
                };
                pointer.relative_motion(&mut data.sudbury, new_point.1, &evt);
            } else {
                error!("Lost our pointer!");
            }
            SudburyInput::finish_pointer_event(&mut data.sudbury, None, serial);
        }
        InputEvent::PointerMotionAbsolute { event: _ } => todo!(),
        InputEvent::PointerButton { event } => {
            let state = wl_pointer::ButtonState::from(event.state());
            let backend_state = match state {
                wl_pointer::ButtonState::Released => backend::input::ButtonState::Released,
                wl_pointer::ButtonState::Pressed => {
                    SudburyInput::finish_pointer_event(
                        &mut data.sudbury,
                        Some(event.button()),
                        serial,
                    );
                    backend::input::ButtonState::Pressed
                }
                _ => todo!(),
            };
            if let Some(pointer) = data.sudbury.seat.get_pointer() {
                let btn = ButtonEvent {
                    serial,
                    time: event.time_msec(),
                    button: event.button_code(),
                    state: backend_state,
                };
                pointer.button(&mut data.sudbury, &btn);
            }
        }
        InputEvent::PointerAxis { event } => {
            let movement: ((f64, f64), (f64, f64)) = match event.axis_source() {
                pointer::AxisSource::Finger => (
                    (
                        // Scroll sequence is terminated with None.
                        event
                            .amount(backend::input::Axis::Horizontal)
                            .unwrap_or_default(),
                        event
                            .amount(backend::input::Axis::Vertical)
                            .unwrap_or_default(),
                    ),
                    (
                        event
                            .amount_discrete(backend::input::Axis::Horizontal)
                            .unwrap_or_default(),
                        event
                            .amount_discrete(backend::input::Axis::Vertical)
                            .unwrap_or_default(),
                    ),
                ),
                pointer::AxisSource::Continuous => (
                    (
                        event
                            .amount(backend::input::Axis::Horizontal)
                            .unwrap_or_default(),
                        event
                            .amount(backend::input::Axis::Vertical)
                            .unwrap_or_default(),
                    ),
                    (
                        event
                            .amount_discrete(backend::input::Axis::Horizontal)
                            .unwrap_or_default(),
                        event
                            .amount_discrete(backend::input::Axis::Vertical)
                            .unwrap_or_default(),
                    ),
                ),
                pointer::AxisSource::Wheel => (
                    (
                        event
                            .amount(backend::input::Axis::Horizontal)
                            .unwrap_or_default(),
                        event
                            .amount(backend::input::Axis::Vertical)
                            .unwrap_or_default(),
                    ),
                    (
                        event
                            .amount_discrete(backend::input::Axis::Horizontal)
                            .unwrap(),
                        event
                            .amount_discrete(backend::input::Axis::Vertical)
                            .unwrap(),
                    ),
                ),
                pointer::AxisSource::WheelTilt => unimplemented!("Use libinput > 1.16"),
            };

            let frame = {
                let input: &SudburyInput = &data.sudbury.input.as_ref().unwrap().borrow();
                input.pointer_axis_frame(event.source(), event.time_msec(), movement.0, movement.1)
            };
            data.sudbury
                .seat
                .get_pointer()
                .unwrap()
                .axis(&mut data.sudbury, frame);
        }
        InputEvent::TouchDown { event: _ } => todo!(),
        InputEvent::TouchMotion { event: _ } => todo!(),
        InputEvent::TouchUp { event: _ } => todo!(),
        InputEvent::TouchCancel { event: _ } => todo!(),
        InputEvent::TouchFrame { event: _ } => todo!(),
        InputEvent::TabletToolAxis { event: _ } => todo!(),
        InputEvent::TabletToolProximity { event: _ } => todo!(),
        InputEvent::TabletToolTip { event: _ } => todo!(),
        InputEvent::TabletToolButton { event: _ } => todo!(),
        InputEvent::Special(_) => todo!(),
        InputEvent::GestureSwipeBegin { event: _ } => todo!(),
        InputEvent::GestureSwipeUpdate { event: _ } => todo!(),
        InputEvent::GestureSwipeEnd { event: _ } => todo!(),
        InputEvent::GesturePinchBegin { event: _ } => todo!(),
        InputEvent::GesturePinchUpdate { event: _ } => todo!(),
        InputEvent::GesturePinchEnd { event: _ } => todo!(),
        InputEvent::GestureHoldBegin { event: _ } => todo!(),
        InputEvent::GestureHoldEnd { event: _ } => todo!(),
    };
}

pub fn initialize_libinput<S>(session: S, eh: &LoopHandle<GlobalState>) -> anyhow::Result<()>
where
    S: Session + 'static + convert::From<S>,
    LibinputSessionInterface<S>: From<S>,
{
    let seat = session.seat();
    let session_interface = LibinputSessionInterface::from(session);
    let mut context = Libinput::new_with_udev(session_interface);
    if let Err(e) = context.udev_assign_seat(&seat) {
        anyhow::bail!("Failed to assign seat {e:?}");
    };
    let backend = LibinputInputBackend::new(context);

    eh.insert_source(backend, input_event)
        .map_err(|e| e.error)
        .with_context(|| "Couldn't enable libinput event generation")?;

    Ok(())
}
