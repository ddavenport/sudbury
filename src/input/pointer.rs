use smithay::{
    backend::renderer::{
        element::{
            surface::{render_elements_from_surface_tree, WaylandSurfaceRenderElement},
            texture::{TextureBuffer, TextureRenderElement},
            AsRenderElements,
        },
        gles2::Gles2Texture,
        ImportAll, ImportMem, Renderer,
    },
    input::pointer::CursorImageStatus,
    output::{Output, WeakOutput},
    render_elements,
    utils::{Logical, Physical, Point, Scale, Transform},
};

static CURSOR: &[u8] = include_bytes!("chrome.rgba");

#[derive(Debug)]
pub struct SudburyPointer {
    status: CursorImageStatus,
    texture: Option<TextureBuffer<Gles2Texture>>,
    loc: Point<f64, Logical>,
    last_output: Option<WeakOutput>,
}

impl SudburyPointer {
    #[allow(dead_code)]
    pub fn new() -> Self {
        Self {
            status: CursorImageStatus::Default,
            texture: None,
            loc: Default::default(),
            last_output: None,
        }
    }

    #[allow(dead_code)]
    pub fn icon<R>(&mut self, renderer: &mut R) -> &TextureBuffer<R::TextureId>
    where
        R: Renderer<TextureId = Gles2Texture> + ImportMem,
    {
        self.texture.get_or_insert_with(|| {
            TextureBuffer::from_memory(
                renderer,
                CURSOR,
                (64i32, 64i32),
                false,
                1,
                Transform::Normal,
                None,
            )
            .expect("Couldn't initialize cursor texture")
        })
    }

    pub fn status(&self) -> &CursorImageStatus {
        &self.status
    }

    pub fn set_status(&mut self, status: CursorImageStatus) {
        self.status = status;
    }

    fn last_output(&self) -> Option<Output> {
        self.last_output.as_ref().and_then(|lo| lo.upgrade())
    }

    pub fn loc(&self) -> (Point<f64, Logical>, Option<Output>) {
        (self.loc, self.last_output())
    }

    fn set_last_output(&mut self, last_output: &Output) {
        self.last_output = Some(last_output.downgrade());
    }

    pub fn set_loc(&mut self, loc: Point<f64, Logical>, out: &Output) {
        self.loc = loc;
        self.set_last_output(out)
    }
}

// A pointer can either be a texture (derived from the compositor itself), or a Wayland surface.
render_elements! {
    pub SudburyPointerElement<R> where R: ImportAll;
    Client=WaylandSurfaceRenderElement<R>,
    Server=TextureRenderElement<<R as Renderer>::TextureId>,
}

impl<R> AsRenderElements<R> for SudburyPointer
where
    R: Renderer<TextureId = Gles2Texture> + ImportAll + ImportMem,
{
    type RenderElement = SudburyPointerElement<R>;

    fn render_elements<E: From<SudburyPointerElement<R>>>(
        &self,
        renderer: &mut R,
        location: Point<i32, Physical>,
        scale: Scale<f64>,
    ) -> Vec<E> {
        match &self.status() {
            CursorImageStatus::Hidden => vec![],
            // Server side pointer
            CursorImageStatus::Default => {
                if let Some(texture) = &self.texture {
                    let t = TextureRenderElement::from_texture_buffer(
                        location.to_f64(),
                        texture,
                        None,
                        None,
                        None,
                    );
                    let pointer = SudburyPointerElement::from(t);
                    vec![pointer.into()]
                } else {
                    panic!("No cursor!");
                }
            }
            // Client side pointer
            CursorImageStatus::Surface(surface) => {
                render_elements_from_surface_tree(renderer, surface, location, scale)
                    .into_iter()
                    .map(E::from)
                    .collect()
            }
        }
    }
}
