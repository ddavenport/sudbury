use smithay::{
    backend::{self, input::KeyState},
    desktop::{Space, Window},
    input::{
        keyboard::{
            keysyms, xkb::keysym_get_name, FilterResult, KeyboardHandle, Keysym, KeysymHandle,
            ModifiersState, XkbConfig,
        },
        pointer::AxisFrame,
    },
    output::Output,
    utils::{Logical, Point, Serial},
    wayland::input_method::InputMethodSeat,
};
use tracing::{debug, error};

#[cfg(feature = "libinput")]
use crate::input::libinput::initialize_libinput;
use crate::{input::pointer::SudburyPointer, Sudbury};

#[cfg(feature = "libinput")]
mod libinput;
pub mod pointer;

#[derive(Debug)]
pub struct SudburyInput {
    _keyboard_handle: KeyboardHandle<Sudbury>,
    pub pointer: SudburyPointer,
    sloppy: bool,
}

#[derive(Debug)]
enum Action {
    Exit,
    CycleFocus,
    Tint,
}

fn to_action(modifiers: ModifiersState, keysym: Keysym) -> Option<Action> {
    let mod_shift = (modifiers.logo, modifiers.shift, modifiers.alt);
    match (mod_shift, keysym) {
        ((true, true, _), keysyms::KEY_E) => Some(Action::Exit),
        ((true, true, _), keysyms::KEY_T) => Some(Action::Tint),
        ((false, false, true), keysyms::KEY_Tab) => Some(Action::CycleFocus),
        _ => None,
    }
}

#[allow(dead_code)]
fn handle_keyboard(
    sudbury: &mut Sudbury,
    state: KeyState,
    modifiers: &ModifiersState,
    keyhand: KeysymHandle,
) -> FilterResult<()> {
    debug!(
        "{state:?} {modifiers:?} {}",
        keysym_get_name(keyhand.modified_sym())
    );

    let Some(action) = to_action(*modifiers, keyhand.modified_sym()) else { return FilterResult::Forward; };

    // If a key was previously intercepted, don't forward release (we didn't forward the press)
    if state == KeyState::Released {
        return FilterResult::Intercept(());
    }

    match action {
        Action::Exit => sudbury.eventloop_handle.1.stop(),
        Action::CycleFocus => todo!(),
        Action::Tint => sudbury.drm_kms.tint(),
    };

    FilterResult::Intercept(())
}

impl SudburyInput {
    /// .
    #[allow(unused_variables)]
    pub fn new(sudbury: &mut Sudbury) -> Option<Self> {
        #[cfg(feature = "libinput")]
        initialize_libinput(
            sudbury.drm_kms.session().clone(),
            sudbury.eventloop_handle(),
        )
        .ok()?;

        let kh = sudbury
            .seat
            .add_keyboard(XkbConfig::default(), 200, 25)
            .ok()?;
        sudbury.seat.add_input_method(XkbConfig::default(), 200, 25);
        sudbury.seat.add_pointer();
        Some(SudburyInput {
            _keyboard_handle: kh,
            pointer: SudburyPointer::new(),
            sloppy: false,
        })
    }

    /// Updates window focus to the pointer location
    ///
    /// Smithay default:
    /// Client only requests a grab, the compositor decides if it grants it or dismisses the popup.
    /// Pointer will continue to send enter/leave for surfaces of the client that the popup surface
    /// belongs to. Keyboard will stay exclusively on popup surface. Clicking on a surface not
    /// equal the client will dismiss the popup compositor side. Clicking on a different surface
    /// from same client will typically destroy popup client side.
    fn keyboard_refocus(sudbury: &mut Sudbury, serial: Serial) {
        // If there's no input, there's no focus
        let Some(inputh) = sudbury.seat.input_method() else { return };
        let Some(keyboard) = sudbury.seat.get_keyboard() else { return; };
        let inputh = inputh.clone();
        // If there is no pointer, it's up to alt+tab switcher to change focus
        let Some(pointer) = sudbury.seat.get_pointer() else { return; };

        // grabbed should be set be one of: movement, dnd, or grab. For any of those, keyboard focus
        // shouldn't be changed.
        // XXX: The inputh logic is inverted from Anvil. Not sure why Anvil works that way.
        if keyboard.is_grabbed() || inputh.keyboard_grabbed() {
            return;
        }
        if pointer.is_grabbed() {
            return;
        }

        // Set keyboard focus to the topmost surface under the pointer.
        //TODO: Check if the client is fullscreen on the output?
        let Some(input) = &sudbury.input else { error!("Input unexpectedly vanished"); return; };
        let (loc, _) = input.borrow().pointer.loc();
        if let Some(element) = sudbury.current_space().element_under(loc) {
            let e = element.0.clone();
            let p = element.1;
            sudbury.current_space_mut().raise_element(&e, true);
            inputh.set_point(&p);
            keyboard.set_focus(sudbury, Some(e), serial);
        }
    }

    /// Truncate a location to the [`Space`].
    ///
    /// Relative mouse movements can push a pointer outside of the space's logical dimensions. This
    /// function will correctt this.
    fn clipper(
        &self,
        space: &Space<Window>,
        loc: Point<f64, Logical>,
    ) -> (Point<f64, Logical>, Option<Output>) {
        if let Some(output) = space.outputs().find(|output| {
            let Some(rect) = space.output_geometry(output) else { return false };
            rect.to_f64().contains(loc)
        }) {
            // If the location is in an output's logical region, nothing to do
            (loc, Some(output.clone()))
        } else {
            // Otherwise, clip to the last known output
            if let Some(rect) = self
                .pointer
                .loc()
                .1
                .as_ref()
                .and_then(|o| space.output_geometry(o))
            {
                (loc.constrain(rect.to_f64()), self.pointer.loc().1)
            } else {
                ((-1.0f64, -1.0f64).into(), None)
            }
        }
    }

    /// Handle pointer potion given the delta
    ///
    /// Aside from handling the pointer location this function will schedule an update for
    /// composition if position has changed.
    ///
    /// Returns the clipped location of the point and the Option<Surface, Point> for Smithay to
    /// update.
    #[allow(clippy::type_complexity)]
    fn pointer_motion(
        sudbury: &mut Sudbury,
        delta: Point<f64, Logical>,
    ) -> (Point<f64, Logical>, Option<(Window, Point<i32, Logical>)>) {
        let Some(input) = &sudbury.input else {
            error!("Input unexpectedly vanished");
            return ((-1.0, -1.0).into(), None);
        };
        let (clipped_loc, out) = {
            let space = sudbury.current_space();
            let input: &SudburyInput = &input.borrow();
            let loc = input.pointer.loc().0 + delta;
            input.clipper(space, loc)
        };

        {
            debug!("Moving to {clipped_loc:?}");
            let input: &mut SudburyInput = &mut input.borrow_mut();
            if let Some(output) = out {
                let (last_loc, last_out) = input.pointer.loc();
                input.pointer.set_loc(clipped_loc, &output);
                if Some(&output) != last_out.as_ref() || last_loc != clipped_loc {
                    sudbury.drm_kms.immediate_composition(&output)
                }
            } else {
                debug!("No valid output after pointer motion?");
            }
        }

        let space = sudbury.current_space();
        (
            clipped_loc,
            space
                .element_under(clipped_loc)
                .map(|(window, point)| (window.clone(), point)),
        )
    }

    /// Must be called after a pointer motion event to adjust keyboard focus
    fn finish_pointer_event(sudbury: &mut Sudbury, button: Option<u32>, serial: Serial) {
        const BTN_MOUSE: u32 = 0x110;
        const BTN_MIDDLE: u32 = 0x112;

        let Some(input) = &sudbury.input else {
            error!("Input unexpectedly vanished");
            return;
        };

        // Sloopy focus just relies on current pointer location
        if input.borrow().sloppy {
            SudburyInput::keyboard_refocus(sudbury, serial);
        } else {
            let Some(button) = button else { return };
            if let BTN_MOUSE..=BTN_MIDDLE = button {
                SudburyInput::keyboard_refocus(sudbury, serial);
            };
        }
    }

    /// Handle pointer axis event
    fn pointer_axis_frame(
        &self,
        source: backend::input::AxisSource,
        time: u32,
        movement_px: (f64, f64),
        movement_abs: (f64, f64),
    ) -> AxisFrame {
        let frame = AxisFrame::new(time)
            .source(source)
            .value(backend::input::Axis::Horizontal, movement_px.0)
            .value(backend::input::Axis::Vertical, movement_px.1)
            .discrete(backend::input::Axis::Horizontal, movement_abs.0 as i32)
            .discrete(backend::input::Axis::Vertical, movement_abs.1 as i32);

        if source == backend::input::AxisSource::Finger {
            frame.stop(backend::input::Axis::Vertical);
            frame.stop(backend::input::Axis::Horizontal);
        }

        frame
    }
}
