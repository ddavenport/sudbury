use smithay::{delegate_primary_selection, wayland::primary_selection::PrimarySelectionHandler};

use crate::Sudbury;

impl PrimarySelectionHandler for Sudbury {
    fn primary_selection_state(
        &self,
    ) -> &smithay::wayland::primary_selection::PrimarySelectionState {
        &self.primary_selection_state
    }
}
delegate_primary_selection!(Sudbury);
