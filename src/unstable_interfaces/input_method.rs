use smithay::delegate_input_method_manager;

use crate::Sudbury;

delegate_input_method_manager!(Sudbury);
