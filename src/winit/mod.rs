use std::{
    borrow::{Borrow, BorrowMut},
    cell::RefCell,
};

use anyhow::Context;
use slog::Logger;
use smithay::{
    backend::{
        renderer::{
            damage::DamageTrackedRenderer, element::surface::WaylandSurfaceRenderElement,
            gles2::Gles2Renderer,
        },
        winit::{self, WinitError, WinitEvent, WinitEventLoop, WinitGraphicsBackend},
    },
    desktop::{self, space::SurfaceTree, Space, Window},
    output::{Mode, Output, PhysicalProperties, Subpixel},
    reexports::wayland_server::DisplayHandle,
    utils::Transform,
};

use tracing::{debug, trace};

use crate::{input::SudburyInput, OutputProperties, Sudbury};

#[derive(Debug)]
pub struct SudburyWinit {
    event_loop: WinitEventLoop,
    backend: WinitGraphicsBackend,
    output: Option<(DamageTrackedRenderer, Output)>,
}

impl SudburyWinit {
    fn handle_events(&mut self, _input: &SudburyInput) -> anyhow::Result<bool> {
        let mut reconfigure = false;
        let r = &mut reconfigure;
        let output = self.output.as_ref().unwrap();
        let res = self
            .event_loop
            .dispatch_new_events(move |event| match event {
                WinitEvent::Resized { size, .. } => {
                    debug!("Winit resize {:?}", size);
                    let mode = Mode {
                        size,
                        refresh: 60_000,
                    };
                    output.1.change_current_state(Some(mode), None, None, None);
                    output.1.set_preferred(mode);
                    (*r) = true;
                }
                WinitEvent::Focus(_) => trace!("Focus event"),
                WinitEvent::Input(_) => trace!("Input event"),
                WinitEvent::Refresh => trace!("Refresh event"),
            });

        if let Err(WinitError::WindowClosed) = res {
            return Ok(false);
        } else {
            res?;
        };

        // Calculate the damage to send to winit somehow
        // let size = self.backend.window_size().physical_size;
        // let _damage = Rectangle::from_loc_and_size((0, 0), size);

        Ok(reconfigure)
    }

    pub fn new(slogger: &Logger) -> Option<Self> {
        // Create a wayland window for our compositor
        let (backend, event_loop) = winit::init(slogger.clone()).ok()?;
        debug!("e {:?}", backend.window_size());

        Some(Self {
            backend,
            event_loop,
            output: None::<(DamageTrackedRenderer, Output)>,
        })
    }
}

impl OutputProperties for SudburyWinit {
    fn enumerate_outputs(
        &mut self,
        display_handle: &DisplayHandle,
        slogger: &slog::Logger,
    ) -> anyhow::Result<()> {
        let mut output = Output::new(
            "output-0".to_string(),
            PhysicalProperties {
                size: (0, 0).into(),
                subpixel: Subpixel::None,
                make: "Smithay".to_string(),
                model: "winit".to_string(),
            },
            slogger.clone(),
        );
        output.create_global::<Sudbury>(display_handle);
        // OpenGL coordinate space is lower left. Smithay renderer accounts for this and
        // transforms to upper left to keep semantics between OpenGL and Vulkan the same. Winit
        // also tries to make this translation, so the flip is to cancel out the one that winit
        // will do.
        output.change_current_state(
            Some(Mode {
                size: self.backend.window_size().physical_size,
                refresh: 60_000,
            }),
            Some(Transform::Flipped180),
            None,
            None,
        );
        output.set_preferred(output.current_mode().expect("Missing mode"));
        self.output = Some((DamageTrackedRenderer::from_output(&output), output));
        Ok(())
    }

    fn render(
        &mut self,
        spaces: &mut [Space<Window>],
        slogger: &slog::Logger,
    ) -> anyhow::Result<()> {
        const AGE: usize = 0;
        const CLEAR_COLOR: [f32; 4] = [0.5, 0.0, 0.25, 1.0];

        self.backend.bind().expect("Bind failed");

        for space in spaces.iter_mut() {
            let output = self.output.as_mut().unwrap();
            desktop::space::render_output::<Gles2Renderer, WaylandSurfaceRenderElement, Window>(
                &output.1,
                self.backend.renderer(),
                AGE,
                &[space],
                &[],
                &mut output.0,
                CLEAR_COLOR,
                &slogger,
            )?;
        }

        Ok(())
    }

    fn swap(&mut self) -> anyhow::Result<()> {
        // None damage will cause plain swap buffers.
        // https://github.com/Smithay/smithay/blob/9d77f1839434e90399329067a33603d76c8c0c15/src/backend/egl/native.rs#L261
        self.backend.submit(None).context("Swap buffer failed")
    }

    fn needs_reconfig(&mut self, input: &SudburyInput) -> anyhow::Result<bool> {
        self.handle_events(input)
    }

    fn iter_out(&self) -> &[Output] {
        self.output
            .as_ref()
            .map(|x| core::slice::from_ref(&x.1))
            .unwrap_or_default()
    }
}
