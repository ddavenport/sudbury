use smithay::delegate_virtual_keyboard_manager;

use crate::Sudbury;

delegate_virtual_keyboard_manager!(Sudbury);
