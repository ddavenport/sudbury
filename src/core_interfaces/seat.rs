use smithay::input::SeatHandler;
use smithay::{
    delegate_seat,
    desktop::Window,
    input::{pointer::CursorImageStatus, Seat},
    wayland::{
        data_device::set_data_device_focus, primary_selection::set_primary_focus,
        seat::WaylandFocus,
    },
};
use tracing::{info, trace};
use wayland_server::Resource;

use crate::Sudbury;

impl SeatHandler for Sudbury {
    type KeyboardFocus = Window;
    type PointerFocus = Window;

    fn seat_state(&mut self) -> &mut smithay::input::SeatState<Self> {
        &mut self.seat_state
    }

    fn focus_changed(&mut self, seat: &Seat<Self>, target: Option<&Window>) {
        let Some(focus) = target else { return; };
        let Some(surface) = focus.wl_surface() else { return; };

        let dh = &self.display_handle;
        let client = match dh.get_client(surface.id()) {
            Ok(c) => Some(c),
            Err(e) => {
                info!("No object {e}");
                None
            }
        };
        trace!("Focus set to {client:?}");
        // set data device and selection focus
        set_data_device_focus(dh, seat, client.as_ref().cloned());
        set_primary_focus(dh, seat, client);
    }

    fn cursor_image(&mut self, _seat: &Seat<Self>, status: CursorImageStatus) {
        if let Some(input) = self.input.as_mut() {
            input.borrow_mut().pointer.set_status(status);
        }
    }
}

delegate_seat!(Sudbury);
