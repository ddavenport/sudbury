use crate::{stable_interfaces::xdg_shell::xdg_configure_window, Sudbury};
use smithay::{
    backend::renderer::utils::on_commit_buffer_handler,
    delegate_compositor,
    desktop::Window,
    reexports::wayland_server::protocol::wl_surface::WlSurface,
    utils::{Logical, Point},
    wayland::compositor::{get_parent, is_sync_subsurface, CompositorHandler},
};

impl Sudbury {
    fn find_window(&self, surface: &WlSurface) -> Option<Window> {
        self.spaces.iter().find_map(|s| {
            s.elements()
                .find(|window| window.toplevel().wl_surface() == surface)
                .cloned()
        })
    }
}

impl CompositorHandler for Sudbury {
    fn compositor_state(&mut self) -> &mut smithay::wayland::compositor::CompositorState {
        &mut self.comp_state
    }

    fn commit(&mut self, surface: &WlSurface) {
        on_commit_buffer_handler(surface);

        let index = *self.seat.user_data().get::<usize>().unwrap();
        let space = &mut self.spaces[index];

        // This will find the first commit for a surface
        if let Some(window) = self
            .orphan_windows
            .iter()
            .find(|window| surface == window.toplevel().wl_surface())
            .cloned()
        {
            // Orphaned windows need to be mapped to an output
            let _toplevel = window.toplevel();
            // TODO: Where should the window be placed?
            space.map_element(window.clone(), Point::<i32, Logical>::from((0, 0)), true);

            // If this is the first commit, the server is expected to send a configure event so
            // that subsequent commits are made aware of things like full screen, desired size, max
            // extents, etc.
            //
            //  https://wayland.app/protocols/xdg-shell#xdg_surface
            //
            //  "After creating a role-specific object and setting it up, the client must perform
            //  an initial commit without any buffer attached. The compositor will reply with an
            //  xdg_surface.configure event. The client must acknowledge it and is then allowed to
            //  attach a buffer to map the surface."
            //
            // TODO: We could be fancy here and try to detect if there is a buffer attached,
            // but why?
            //
            // TODO2: What about non-XDG windows?
            xdg_configure_window(self.drm_kms.outputs().first().unwrap(), &window, surface);

            // Remove the window we just mapped.
            self.orphan_windows.retain(|w| *w != window);
        }

        if self.popups.find_popup(surface).is_some() {
            todo!("Handle popups")
        }

        let output = match self
            .seat
            .get_pointer()
            .and_then(|ph| space.output_under(ph.current_location()).next().cloned())
        {
            Some(o) => o,
            None => space.outputs().next().cloned().unwrap(),
        };

        if !is_sync_subsurface(surface) {
            let mut root = surface.clone();
            while let Some(parent) = get_parent(&root) {
                root = parent;
            }
            if let Some(window) = self.find_window(&root) {
                window.on_commit();
            }
        }

        self.drm_kms.schedule_idle_composition(&output);
    }
}
delegate_compositor!(Sudbury);
