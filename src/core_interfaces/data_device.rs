use smithay::{
    delegate_data_device,
    wayland::data_device::{
        ClientDndGrabHandler, DataDeviceHandler, DataDeviceState, ServerDndGrabHandler,
    },
};

use crate::Sudbury;

impl DataDeviceHandler for Sudbury {
    fn data_device_state(&self) -> &DataDeviceState {
        &self.data_device_state
    }
}

impl ClientDndGrabHandler for Sudbury {}
impl ServerDndGrabHandler for Sudbury {}

delegate_data_device!(Sudbury);
