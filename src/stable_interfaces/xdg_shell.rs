use crate::Sudbury;
use smithay::{
    delegate_xdg_shell,
    desktop::Window,
    output::Output,
    reexports::{
        wayland_protocols::xdg::shell::server::xdg_toplevel,
        wayland_server::protocol::{wl_seat::WlSeat, wl_surface::WlSurface},
    },
    utils::Serial,
    wayland::{
        compositor::with_states,
        shell::xdg::{
            PopupSurface, PositionerState, ToplevelSurface, XdgShellHandler, XdgShellState,
            XdgToplevelSurfaceRoleAttributes,
        },
    },
};
use std::sync::Mutex;

impl XdgShellHandler for Sudbury {
    fn xdg_shell_state(&mut self) -> &mut XdgShellState {
        &mut self.xdg_state
    }

    fn new_toplevel(&mut self, surface: ToplevelSurface) {
        let window = Window::new(surface);
        self.orphan_windows.push(window);
    }

    fn new_popup(&mut self, _surface: PopupSurface, _positioner: PositionerState) {
        todo!()
    }

    fn grab(&mut self, _surface: PopupSurface, _seat: WlSeat, _serial: Serial) {
        todo!()
    }
}

delegate_xdg_shell!(Sudbury);

/// Configure an XDG window for the first time. This is
///
/// # Panics
///
/// Panics if .
pub fn xdg_configure_window(output: &Output, window: &Window, surface: &WlSurface) {
    let toplevel = &window.toplevel();
    // the data_map is used to store the initial configuration status. This is copied from
    // smallvil/anvil.
    let initial_configure_sent = with_states(surface, |states| {
        states
            .data_map
            .get::<Mutex<XdgToplevelSurfaceRoleAttributes>>()
            .unwrap()
            .lock()
            .unwrap()
            .initial_configure_sent
    });

    if !initial_configure_sent {
        // Window will start on the first output, full sized
        toplevel.with_pending_state(|state| {
            state.states.set(xdg_toplevel::State::Resizing);
            state.size = Some(output.preferred_mode().unwrap().size.to_logical(1));
        });
        toplevel.send_configure();
    }
}
#[allow(unused)]
pub fn xdg_configure_popup() {
    todo!()
}
