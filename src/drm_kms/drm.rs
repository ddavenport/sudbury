use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    os::fd::{FromRawFd, IntoRawFd},
    path::PathBuf,
    rc::Rc,
    sync::Mutex,
};

use anyhow::Context;
use smithay::{
    backend::{
        allocator,
        drm::{DrmDevice, DrmDeviceFd, DrmDeviceNotifier, DrmNode},
        egl::{native::EGLSurfacelessDisplay, EGLContext, EGLDevice, EGLDisplay},
        renderer::{
            element::{
                surface::WaylandSurfaceRenderElement, AsRenderElements, RenderElementStates,
            },
            gles2::{Gles2Renderbuffer, Gles2Renderer},
            ImportAll, ImportDma,
        },
        session::Session,
    },
    desktop::{
        space::{self, SpaceRenderElements},
        Space, Window,
    },
    input::pointer::{CursorImageAttributes, CursorImageStatus},
    output::{Output, WeakOutput},
    reexports::{gbm, nix::fcntl::OFlag, wayland_server::DisplayHandle},
    render_elements,
    utils::{DeviceFd, Scale},
    wayland::{self, dmabuf::DmabufState},
};
use tracing::trace;

use crate::{
    drm_kms::kms::SudburyKms,
    input::{pointer::SudburyPointerElement, SudburyInput},
    Sudbury, CLEAR_COLOR, FULLSCREEN_CLEAR_COLOR,
};

/// Helper to determine if a client is fullscreen on [`Output`]
fn find_fullscreen<'a>(spaces: &'a [&Space<Window>], output: &Output) -> Option<&'a Window> {
    for space in spaces.iter() {
        for window in space.elements() {
            if space.outputs_for_element(window).contains(output) {
                let toplevel = window.toplevel();
                if toplevel.current_state().fullscreen_output.is_some() {
                    return Some(window);
                }
            }
        }
    }
    None
}

/// State for the renderer used by [`Sudbury`]
#[derive(Debug)]
pub struct SudburyDrmCompositor {
    renderer: Gles2Renderer,
    dmabuf_state: DmabufState,
    /// A list of formats that are supported by both the renderer and display
    formats: HashSet<allocator::Format>,
    pub(crate) damage: HashMap<WeakOutput, RenderElementStates>,
    input: Option<Rc<RefCell<SudburyInput>>>,
}

/// Create a handle to a drm device.
///
/// Failure is determined mostly by the session. For null sessions, failure is most likely due to
/// not being first and therefore not getting DRM master.
fn create_drm_device<T: Session>(
    session: &mut T,
    drm_node: PathBuf,
) -> anyhow::Result<(DrmDevice, DrmDeviceNotifier)>
where
    <T as Session>::Error: std::marker::Send,
    <T as Session>::Error: std::error::Error,
    <T as Session>::Error: Sync,
    <T as Session>::Error: 'static,
{
    let fd = session
        .open(
            &drm_node,
            OFlag::O_RDWR | OFlag::O_CLOEXEC | OFlag::O_NOCTTY | OFlag::O_NONBLOCK,
        )
        .with_context(|| "Failed to open {drm_node}")?
        .into_raw_fd();

    let device_fd: DeviceFd = unsafe { DeviceFd::from_raw_fd(fd) };
    let device_fd = DrmDeviceFd::new(device_fd);

    let drm = DrmDevice::new(device_fd, true).with_context(|| "Couldn't access {drm_node}")?;

    // AFAIK, there's no reason to make atomic drivers exclusive.
    if !drm.0.is_atomic() {
        unimplemented!("Legacy modestting implementations aren't supported");
    }

    Ok(drm)
}

/// Create an EGL context and GBM device for doing compositor GPU operations
///
/// Optionally create a handle to a render node. Render nodes are currently unused by Sudbury.
fn create_egl_context(
    fd: &DrmDeviceFd,
    render_node: bool,
) -> anyhow::Result<(gbm::Device<DrmDeviceFd>, EGLContext, Option<DrmNode>)> {
    // Use the open file descriptor to get a GbmDevice
    let gbm = gbm::Device::new(fd.clone()).with_context(|| "Couldn't create a GBM device")?;

    // Create an EGLDisplay using
    // `https://registry.khronos.org/EGL/extensions/MESA/EGL_MESA_platform_surfaceless.txt`
    let egl_display = EGLDisplay::new::<EGLSurfacelessDisplay>(EGLSurfacelessDisplay)
        .with_context(|| "Couldnt create an EGLDisplay")?;

    // Obtain the [Render Node](https://www.kernel.org/doc/html/latest/gpu/drm-uapi.html#render-nodes).
    // Sudbury doesn't currently use the render node.
    let render_node = if render_node {
        let egl_device = EGLDevice::device_for_display(&egl_display)
            .with_context(|| "Not EGLDisplay for device")?;
        trace!(device = ?egl_device);

        let render_node = egl_device.try_get_render_node()?;
        trace!(?render_node);
        render_node
    } else {
        None
    };

    let context = EGLContext::new(&egl_display).with_context(|| "Couldn't create EGL context")?;
    trace!(?context);

    Ok((gbm, context, render_node))
}

/// Initialize [`DmabufState`] for the renderer.
fn init_render_dmabuf(
    display_handle: &DisplayHandle,
    renderer: &Gles2Renderer,
) -> (DmabufState, Vec<allocator::Format>) {
    // Could also obtain the dmabuf format list from EGL.dmabuf_render_formats(). The list should
    // be identical to the below.
    let formats: Vec<allocator::Format> = renderer
        .dmabuf_formats()
        .map(|format| format.to_owned())
        .collect();
    let mut dmabuf_state = DmabufState::new();
    dmabuf_state.create_global::<Sudbury>(display_handle, formats.to_vec());
    (dmabuf_state, formats)
}

render_elements! {
    SudburyRenderElements<R, E> where R: ImportAll;
    Space=SpaceRenderElements<R, E>,
    Window=WaylandSurfaceRenderElement<R>,
    Pointer=SudburyPointerElement<R>,
}

impl SudburyDrmCompositor {
    /// Handler for new DRM devices
    ///
    /// Enumeration creates and instantiates all the relevant state for rendering the compositor's
    /// frames. The general operations is:
    /// 1. Create a [`DrmDevice`] from the current session
    /// 2. Create a [`gbm::Device`] from the [`DrmDevice`]'s fd.
    /// 3. Create an [`EGLDisplay`] from the [`gbm::Device`]. This is used for eglSwapBuffer.
    /// 4. Create an [`EGLContext`] from the [`EGLDisplay`]. EGLContexts are used for rendering
    ///    operations. The backing EGLDevice's GbmDevice will be the backend for surfaces.
    /// 5. Create a [`Gles2Renderer`] with the initialized [`EGLContext`]
    /// 6. Enumerate and initialize all existing outputs
    /// 7. Set up dispatcher for DRM events ("vblank")
    /// 8. Create the global [`DmabufState`] and bind it to the renderer.
    pub fn new<T: Session>(
        dh: &DisplayHandle,
        session: &mut T,
        path: PathBuf,
    ) -> Option<(
        Self,
        (DrmDevice, DrmDeviceNotifier),
        gbm::Device<DrmDeviceFd>,
    )>
    where
        <T as Session>::Error: std::marker::Send,
        <T as Session>::Error: std::error::Error,
        <T as Session>::Error: Sync,
        <T as Session>::Error: 'static,
    {
        // The DRM device is created here. It will be owned by the [`calloop::Dispatcher`].
        let device = create_drm_device(session, path).ok()?;
        trace!(?device);
        //
        let (gbm_device, egl_context, _render_node) =
            create_egl_context(device.0.device_fd(), false).ok()?;
        trace!(?egl_context);

        let egl_formats = egl_context.display().dmabuf_render_formats().to_owned();

        // We can either enumerate all nodes that can be an EGLDevice, or create one from the
        // EGLDisplay. Doing the latter has the benefit of likely having format compatibility between
        // render and display. In the current smithay APIs, there is no way to get a format list of
        // EGLDevices such that you can negotiate formats anyway unless there is a way to match the
        // gbm device with the EGLDevice

        #[cfg(not(feature = "use-system-lib"))]
        let renderer = unsafe { Gles2Renderer::new(egl_context) }.ok()?;
        #[cfg(feature = "use-system-lib")]
        let renderer = {
            use smithay::backend::renderer::ImportEgl;
            let mut renderer = unsafe { Gles2Renderer::new(egl_context) }.ok()?;
            renderer.bind_wl_display(dh).ok()?;
            renderer
        };

        let (dmabuf_state, renderer_formats) = init_render_dmabuf(dh, &renderer);

        let renderer_formats = HashSet::from_iter(renderer_formats);
        trace!("DMABUF init complete. Formats = {renderer_formats:?}");

        // Get the intersection of formats supported by EGLDisplay and the renderer
        let formats = renderer_formats.intersection(&egl_formats);

        let diff: Vec<_> = renderer_formats
            .symmetric_difference(&egl_formats)
            .collect();
        if !diff.is_empty() {
            trace!("Format mismatch between GLES2 and EGL {:#?}", diff)
        }

        // It'd be nice to have this module own the DRM device, but it must be handed off to the
        // calloop dispatcher.
        //
        // Since KMS will own the GbmBufferedSurface, let that own thee gbm device.
        Some((
            Self {
                renderer,
                dmabuf_state,
                formats: formats.cloned().collect(),
                damage: HashMap::with_capacity(3), // Reasonable #outputs
                input: None,
            },
            device,
            gbm_device,
        ))
    }

    /// Composites surfaces for [`Output`]
    ///
    /// # Errors
    ///
    /// This function will return an error if .
    pub(super) fn render(
        &mut self,
        kms: &mut SudburyKms,
        output: &Output,
        spaces: &[Space<Window>],
        current: usize,
    ) -> anyhow::Result<(bool, RenderElementStates)> {
        let renderer = kms.scanout_data(output);

        let scale: Scale<f64> = output.current_scale().fractional_scale().into();

        let (space, _current) = spaces
            .get(current)
            .map_or_else(|| (spaces.get(0).expect("Lost space"), 0), |s| (s, current));

        // TODO: Only draw the pointer if it lives in this output, check inp.pointer.last_output
        let mut elements: Vec<SudburyRenderElements<Gles2Renderer, _>> =
            self.input.as_ref().map_or(vec![], |input| {
                let inp: &SudburyInput = &RefCell::borrow(input);

                // Hotspot is handled for us by Smithay
                let hotspot = if let CursorImageStatus::Surface(ref surface) = inp.pointer.status()
                {
                    wayland::compositor::with_states(surface, |states| {
                        states
                            .data_map
                            .get::<Mutex<CursorImageAttributes>>()
                            .unwrap()
                            .lock()
                            .unwrap()
                            .hotspot
                    })
                } else {
                    (0, 0).into()
                };

                // This logic is copied from Anvil.
                let loc = inp.pointer.loc().0
                    - space.output_geometry(output).unwrap().loc.to_f64()
                    - hotspot.to_f64();
                inp.pointer.render_elements(
                    &mut self.renderer,
                    loc.to_physical_precise_round(scale),
                    scale,
                )
            });

        // Handle all fullscreen windows first
        let cc = if let Some(fullscreen) = find_fullscreen(&[space], output) {
            elements.extend(AsRenderElements::<Gles2Renderer>::render_elements(
                fullscreen,
                &mut self.renderer,
                (0, 0).into(),
                scale,
            ));
            FULLSCREEN_CLEAR_COLOR
        } else {
            elements.extend(
                space::space_render_elements::<Gles2Renderer, Window, _>(
                    &mut self.renderer,
                    spaces,
                    output,
                )
                .unwrap_or_default()
                .into_iter()
                .map(SudburyRenderElements::Space),
            );
            CLEAR_COLOR
        };
        let result =
            renderer.render_frame::<_, _, Gles2Renderbuffer>(&mut self.renderer, &elements, cc)?;
        Ok((result.damage.is_some(), result.states))
    }

    pub fn dmabuf_state_mut(&mut self) -> &mut DmabufState {
        &mut self.dmabuf_state
    }

    pub fn renderer_mut(&mut self) -> &mut Gles2Renderer {
        &mut self.renderer
    }

    pub fn formats(&self) -> &HashSet<allocator::Format> {
        &self.formats
    }

    pub fn set_input(&mut self, input: Rc<RefCell<SudburyInput>>) {
        self.input = Some(input);
    }
}
