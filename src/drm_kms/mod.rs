//! An abstraction over a renderer based on DRM with KMS operations.
//!
//! The DRM renderer encapsulates GLES2 or Vulkan based GPU rendering, while KMS would be either
//! EGL or Vulkan WSI. The GLES2 renderer has several extensions as dependencies.
//!
//! TODO: Support Vulkan
use anyhow::Context;
#[cfg(feature = "libseat")]
use smithay::backend::session::libseat::LibSeatSession;
use smithay::{
    backend::{
        allocator::dmabuf::Dmabuf,
        drm::{DrmError, DrmEvent, DrmEventMetadata},
        renderer::{
            element::{self, RenderElementStates},
            DebugFlags, ImportDma,
        },
        session::Session,
        udev::{UdevBackend, UdevEvent},
    },
    desktop::{
        utils::{
            self, surface_presentation_feedback_flags_from_states, surface_primary_scanout_output,
            OutputPresentationFeedback,
        },
        Space, Window,
    },
    output::{Output, WeakOutput},
    reexports::{
        calloop::{self, timer::Timer, RegistrationToken},
        wayland_server::DisplayHandle,
    },
    wayland::dmabuf::DmabufState,
};
use std::{cell::RefCell, env, path::PathBuf, rc::Rc, time::Duration};
use tracing::{debug, debug_span, error, info, instrument, trace, warn};

#[cfg(feature = "tty-session")]
use crate::session::direct::DirectSession;
#[cfg(not(any(feature = "tty-session", feature = "libseat")))]
use crate::session::null::NullSession;
use crate::{drm_kms::kms::SudburyKms, input::SudburyInput, GlobalState, Sudbury};
#[cfg(any(feature = "tty-session", feature = "libseat"))]
use smithay::backend::session::Event::{ActivateSession, PauseSession};

use self::drm::SudburyDrmCompositor;

mod drm;
mod kms;

/// State of [`SudburyDrmRenderer`] device, and [`SudburyKms`] controls
///
/// It's kind of a midlayer. Look the other way. Eventually this could become a trait if it were
/// desirable
#[derive(Debug)]
#[allow(dead_code)]
pub struct SudburyDrmKms {
    /// Since input is currently optional, this module owns the session, but it will need to be
    /// shared for input/seat.
    #[cfg(feature = "libseat")]
    session: LibSeatSession,
    #[cfg(feature = "tty-session")]
    session: DirectSession,
    #[cfg(not(any(feature = "tty-session", feature = "libseat")))]
    session: NullSession,

    /// Eventloop handle created by [`Sudbury`]
    loop_handle: calloop::LoopHandle<'static, GlobalState>,

    /// The DRM devices
    ///
    /// The DRM device provides an abstraction for GPU composition. Sudbury currently only supports
    /// a single DRM device.
    dev: SudburyDrmCompositor,
    /// KMS functionality
    kms: SudburyKms,
    /// Dispatcher for DRM events.
    token: calloop::RegistrationToken,

    /// Token for the idle render
    ///
    /// When there are active clients, VBlank is driving repaints. However, when clients become
    /// inactive something needs to restart the compositors state machine. This token is [`Some`]
    /// when a client has committed a buffer after this idle period. Once the event loop idles, it
    /// will repaint and the state will go back to VBlank driven.
    ///
    /// FIXME: This probably needs to be a per output thing.
    render_idle: RefCell<Option<calloop::Idle<'static>>>,

    /// Token for current scheduled composting
    ///
    /// All composition is scheduled in a deferred fashion. There are at least 3 ways in which
    /// composition will be scheduled.
    /// 1. VBlank - some time after vblank (giving clients a chance to run)
    /// 2. After idle - When all clients go idle, a commit request will schedule composition.
    /// 3. Mouse pointer movement
    ///
    /// Since the 3 events are asynchronous with respect to the origin, this token is used to
    /// avoid over or under compositin.
    render_token: Option<RegistrationToken>,
}

/// Calculates microseconds until compositor should try to composite based on scale.
macro_rules! to_micros {
    ($e:expr, $scale:expr) => {
        Duration::from_micros(((1000000 as f32 / $e as f32) * $scale) as u64)
    };
}

/// Number of microseconds until the estimated next VBlank
macro_rules! next_frame {
    ($refresh:expr) => {
        calloop::timer::TimeoutAction::ToDuration(to_micros!($refresh, 1.0))
    };
}

/// Send frame callbacks to elements in the [`Space`] for the given [`Output`]
///
/// [`RenderElementStates`] contains whether or not the given element is occluded, and frame
/// callbacks shall only be sent to the ones which have visible area.
///
/// See: [`Window::send_frame`]
///
fn prep_frame_callbacks(space: &Space<Window>, output: &Output, states: &RenderElementStates) {
    for window in space.elements() {
        window.with_surfaces(|surface, surface_data| {
            utils::update_surface_primary_scanout_output(
                surface,
                output,
                surface_data,
                states,
                element::default_primary_scanout_output_compare,
            );
        });
        // TODO: Consider a different FnOnce for selecting based on the slowest output
        if space.outputs_for_element(window).contains(output) {
            crate::MONO_CLOCK.with(|clock| {
                window.send_frame(
                    output,
                    clock.now(),
                    None,
                    utils::surface_primary_scanout_output,
                );
            });
        }
    }
}

impl SudburyDrmKms {
    /// Callback for Udev events.
    ///
    /// Dynamic GPU changes are currently unsupported. eGPU will change this need.
    #[instrument]
    fn udev_callback(event: UdevEvent, _meta: &mut (), _data: &mut GlobalState) {
        match event {
            UdevEvent::Added {
                device_id: _,
                path: _,
            } => {
                unimplemented!("dynamic GPU addition unsupported")
            }
            UdevEvent::Changed { device_id: _ } => todo!(),
            UdevEvent::Removed { device_id: _ } => {
                unimplemented!("dynamic GPU removal unsupported")
            }
        }
    }

    /// Issue render and frame callbacks.
    ///
    /// This is the first order entry point and holistic function for composition. It will:
    /// 1. Use the GPU to composite all windows for the given [`Output`].
    /// 2. If the composition caused damage, queue a swap to the next buffer
    /// 3. Issue the frame callbacks to the clients
    /// 4. Refresh internal Smithay state.
    ///
    /// TODO: Consider multiplexing spaces and current. Originally, all spaces were needed for
    /// nested functions but that requirement has gone away.
    fn __composite(
        &mut self,
        spaces: &mut [Space<Window>],
        current: usize,
        output: &Output,
    ) -> anyhow::Result<bool> {
        debug!("Render");

        // The lowest level function never shall mess with the render_token.
        assert!(self.render_token.is_none());

        let (damage, element_states) = self.dev.render(&mut self.kms, output, spaces, current)?;

        if damage {
            let mut feedback = OutputPresentationFeedback::new(output);
            for window in spaces[current].elements() {
                window.take_presentation_feedback(
                    &mut feedback,
                    surface_primary_scanout_output,
                    |s, _| surface_presentation_feedback_flags_from_states(s, &element_states),
                );
            }
            debug!("Swapbuffer");
            self.kms.swap(output, feedback)?;
        }

        self.dev.damage.insert(output.downgrade(), element_states);

        // Per smithay, refresh needs to be called periodically to update internal state. Since
        // tracking for when spaces go active/inactive isn't happening currently, call it for all
        // spaces.
        spaces.iter_mut().for_each(|s| s.refresh());
        Ok(damage)
    }

    /// Choose compositor vs. client rendering split.
    fn select_balance() -> f32 {
        const DEFAULT_BALANCE: f32 = 0.75;
        let render_balance: f32 = env::var("SBRY_RENDER_DIVIDE").map_or_else(
            |_v| DEFAULT_BALANCE,
            |var| var.parse::<f32>().unwrap_or(DEFAULT_BALANCE),
        );
        render_balance.clamp(0.01, 0.99)
    }

    /// Schedule a vblank for what would be the next vblank.
    ///
    /// Vblank events are only generated when there is work (even though in hardware a VBLANK will
    /// occur every frame). For going into idle, a last vblank is scheduled for the future even
    /// though DRM will not be generating one.
    ///
    /// Returns the registration token for the fake vblank event along with the [`TimeoutAction`]
    fn fake_vblank(
        sudbury: &mut Sudbury,
        output: &Output,
    ) -> (Option<RegistrationToken>, calloop::timer::TimeoutAction) {
        let refresh = output.current_mode().map_or(60, |m| m.refresh);
        let balance = Self::select_balance();
        let fblank = Timer::from_duration(to_micros!(refresh, 1.0 - balance));
        trace!(
            "Scheduling fake vblank ({refresh}fps {:?})",
            to_micros!(refresh, 1.0 - balance)
        );
        let cloned_out = output.clone();

        // XXX: At this point there should be no idle events. If a client commits a new surface,
        // a new idle event will be scheduled unnecessarily, see [`schedule_idle_composition`]. The
        // result is an extra frame will be damaged and composited; so it should be fixed but it's
        // not a big deal.

        // Per above, clients are going to go idle now, so clear the idle "mutex"
        if let Some(render_idle) = sudbury.drm_kms.render_idle.take() {
            debug!("Idling render");
            render_idle.cancel();
        }

        // The fake vblank scheduling had to have been called by a real event that tried to
        // composite. That code path will make sure the rendering token is cleaned up before
        // getting here.
        let old_token = sudbury.drm_kms.render_token.take();
        assert!(old_token.is_none());

        // Schedule a re-render on what would roughly be the next vblank.
        match sudbury.drm_kms.loop_handle.clone().insert_source(
            fblank,
            move |_, _, data: &mut GlobalState| {
                // When this fake vblank event was scheduled, someone had to have the token.
                let old_token = data.sudbury.drm_kms.render_token.take();
                assert!(old_token.is_some());

                let sudbury = &mut data.sudbury;
                let space = sudbury.current_space_no();
                match sudbury
                    .drm_kms
                    .__composite(&mut sudbury.spaces, space, &cloned_out)
                {
                    // This will start normal VBlank driven rendering back up.
                    Ok(damaged) => {
                        trace!("Fake vblank damage: {damaged}");
                        calloop::timer::TimeoutAction::Drop
                    }
                    // Try again.
                    // TODO: Should limit number of tries.
                    Err(e) => {
                        error!("Failed to render fake vblank {e}");
                        next_frame!(refresh)
                    }
                }
            },
        ) {
            // The last composition is done. Drop the timer and wait for new events.
            Ok(token) => (Some(token), calloop::timer::TimeoutAction::Drop),

            // An error occured, try again
            Err(e) => {
                error!("{e}: Rescheduling fake vblank");
                (None, next_frame!(refresh))
            }
        }
    }

    /// Try to composite the specified output
    fn composite(
        sudbury: &mut Sudbury,
        output: &WeakOutput,
        refresh: usize,
    ) -> calloop::timer::TimeoutAction {
        let span = debug_span!("render");
        let _enter = span.enter();
        let Some(output) = output.upgrade() else {
            return next_frame!(refresh);
        };

        // The old token should be the one that scheduled this composition. If it's none, how did
        // we get here?
        //
        let old_token = sudbury.drm_kms.render_token.take();
        assert!(old_token.is_some());

        // The most common case we enter this when one or more clients have causing damage
        // and this is called as part of the VBlank handling. This is the `else` case.
        //
        // The less common case is rendering is scheduled while the compositor is idle.
        // This is called in response to [core_interfaces::compositor::commit]. Generally a
        // client will:
        // 1. attach buffer
        // 2. damage buffer
        // 3. request frame callback
        // 4. commit
        //
        // In that case, the scheduled render will have damage and things will look much
        // the same as the common case (going to `else`). However, a frame callback may be
        // requested without an attached buffer/damage. This case will have no damage
        // reported. This case will go to the `if`. This turns out to be the case for
        // all/most XDG aware clients as they request a frame callback with no buffer
        // attached in order to obtain the XDG configure event. BUT, if the client does
        // attach a buffer before this handler runs (which is common), we're back to the
        // `else` case. Additionally a client's last commit will also potentially yield no
        // damage.
        //
        // In any of the above cases, `render_idle` acts as a mutex. It's locked on the
        // first idle render, and released when the last client has no damage.
        let space = sudbury.current_space_no();
        let (token, time) = if sudbury
            .drm_kms
            .__composite(&mut sudbury.spaces, space, &output)
            .map_or(false, |d| !d)
        {
            Self::fake_vblank(sudbury, &output)
        } else {
            // Timers are always one shot
            (None, calloop::timer::TimeoutAction::Drop)
        };

        // Set the new token to be the fake vblank if there is one, or None if we're done.
        sudbury.drm_kms.render_token = token;
        time
    }

    /// Normal rendering event
    ///
    /// In order to provide clients and opportunity to render within the given raster period, the
    /// compositor is deferred by some amount, see XXX. This is the handler when the compositor can
    /// actually go ahead and render.
    ///
    /// It should only be scheduled if there was damage in the last period and is generally going
    /// to be scheduled after a VBlank, or, if the compositor is entirely idle the wl_compositor
    /// commit request will.
    ///
    /// Fails if the rendering fails to schedule
    fn defer_composition(&mut self, output: WeakOutput) -> anyhow::Result<()> {
        // If there is a request to defer composition, but there is already a render scheduled
        // just let that take its course
        if self.render_token.is_some() {
            trace!("Skip deferred composition");
            return Ok(());
        }

        let balance = Self::select_balance();
        let refresh = output
            .upgrade()
            .map_or(60, |o| o.current_mode().map_or(60, |m| m.refresh));
        let timer = Timer::from_duration(to_micros!(refresh, balance));

        trace!(
            "Scheduling render ({}fps {:?})",
            refresh,
            to_micros!(refresh, balance)
        );

        // Sets up rendering for some point in the future. The token returned here is that of the
        // composition thread, which may actual schedule a fake vblank in the future.
        let token = self
            .loop_handle
            .insert_source(timer, move |_, _, data: &mut GlobalState| {
                Self::composite(&mut data.sudbury, &output, refresh as usize)
            })
            .map_err(|e| e.error)?;

        assert!(self.render_token.is_none());
        self.render_token = Some(token);

        Ok(())
    }

    /// Schedule composition for some future time for [`Output`]
    ///
    /// This is useful when some event wishes to lazily wake up the rendering part of composition.
    pub fn schedule_idle_composition(&mut self, output: &Output) {
        // If clients haven't been doing anything, there are no timers active for doing the render.
        // On the next idle, kick of a composite render.
        //
        // Note that the idle renderer is set to NULL on the very next render, but this event will
        // only trigger when everything is idle.
        let weak_out = output.downgrade();
        if self.render_idle.borrow().is_none() {
            let span = debug_span!("idle");
            let _guard = span.enter();
            let idle = self.loop_handle.insert_idle(move |data: &mut GlobalState| {
                trace!("Scheduling idle render");
                if let Err(e) = data.sudbury.drm_kms.defer_composition(weak_out) {
                    error!("Can't handle. Things may stop working {e}");
                }
            });
            self.render_idle.replace(Some(idle));
        } else {
            trace!("Skipping idle render");
        }
    }

    pub fn immediate_composition(&mut self, output: &Output) {
        if self.render_token.is_some() {
            return;
        }

        let weak_out = output.downgrade();
        _ = self
            .loop_handle
            .insert_source(Timer::immediate(), move |_, _, data| {
                // It's possible an event was already queued and snuck in before the immediate
                // timer in which case, there's nothing to do.
                if data.sudbury.drm_kms.render_token.is_some() {
                    return calloop::timer::TimeoutAction::Drop;
                }
                if let Some(output) = weak_out.upgrade() {
                    let space = data.sudbury.current_space_no();
                    if let Err(e) =
                        data.sudbury
                            .drm_kms
                            .__composite(&mut data.sudbury.spaces, space, &output)
                    {
                        error!("Failed to composite {e}");
                    }
                }
                calloop::timer::TimeoutAction::Drop
            });
    }

    /// Handler for [`DrmEvent`]
    ///
    /// VBlank events will schedule rendering for the next frame.
    fn drm_event(event: DrmEvent, meta: &mut Option<DrmEventMetadata>, data: &mut GlobalState) {
        match event {
            DrmEvent::VBlank(crtc) => {
                debug!("VBLANK");
                // Let Smithay know that we're done with the frame
                let Some(output) = ({
                    let kms = &mut data.sudbury.drm_kms.kms;
                    kms.vblank(crtc, meta.as_mut().unwrap()) }) else { return; };

                // Schedule rendering of the next frame
                let weak_out = output.downgrade();
                if let Some(element_states) = data.sudbury.drm_kms.dev.damage.remove(&weak_out) {
                    debug!("Issue frame callbacks");
                    let spaces = &data.sudbury.spaces;
                    let current = data.sudbury.current_space_no();
                    prep_frame_callbacks(&spaces[current], &output, &element_states);
                }
                match data.sudbury.drm_kms.defer_composition(weak_out) {
                    Ok(_) => {
                        trace!("Scheduling render at {:?}", meta.as_ref().unwrap())
                    }
                    Err(e) => {
                        error!("Failed to schedule render {e}");
                        todo!("Need to issue frame callbacks");
                        // If the render failed to schedule a client is likely to be waiting for a
                        // frame callback, so send them now even though we didn't successfully
                        // composite the last frame.
                    }
                };
            }
            DrmEvent::Error(e) => match e {
                DrmError::DrmMasterFailed => todo!(),
                DrmError::Access {
                    errmsg: _,
                    dev: _,
                    source: _,
                } => todo!(),
                DrmError::UnableToGetDeviceId(_) => todo!(),
                DrmError::DeviceInactive => todo!(),
                DrmError::ModeNotSuitable(_) => todo!(),
                DrmError::CrtcAlreadyInUse(_) => todo!(),
                DrmError::SurfaceWithoutConnectors(_) => todo!(),
                DrmError::PlaneNotCompatible(_, _) => todo!(),
                DrmError::NonPrimaryPlane(_) => todo!(),
                DrmError::NoSuitableEncoder {
                    connector: _,
                    crtc: _,
                } => todo!(),
                DrmError::UnknownProperty { handle: _, name: _ } => todo!(),
                DrmError::TestFailed(_) => todo!(),
                // The following should only happen with legacy modesetting which we disallow for
                // now.
                DrmError::NoPlane => panic!("How"),
                DrmError::NoFramebuffer(_) => panic!("Are We"),
                DrmError::UnsupportedPlaneConfiguration(_) => panic!("Here?"),
            },
        };
    }

    pub fn new(
        display_handle: &DisplayHandle,
        loop_handle: &calloop::LoopHandle<'static, GlobalState>,
        path: PathBuf,
        space: &mut Space<Window>,
    ) -> Option<Self> {
        #[cfg(feature = "libseat")]
        let Ok((mut session, notifier)) = LibSeatSession::new() else {
            error!("Failed to create session");
            return None;
        };
        #[cfg(feature = "tty-session")]
        let Ok((mut session, notifier)) = DirectSession::new(None) else {
            error!("Failed to create session");
            return None;
        };

        #[cfg(not(any(feature = "tty-session", feature = "libseat")))]
        let mut session = NullSession::new();

        let Ok(udev_be) = UdevBackend::new(session.seat()) else {
            error!("Failed to create Udev backend");
            return None;
        };

        let dispatch = calloop::Dispatcher::new(udev_be, Self::udev_callback);
        if let Err(e) = loop_handle.register_dispatcher(dispatch) {
            error!("{e}");
            return None;
        };

        let Some((sbry_renderer, drm_device, gbm_device)) =
        SudburyDrmCompositor::new(display_handle, &mut session, path) else {
            error!("Failed to create the renderer");
            return None;
        };

        let kms = SudburyKms::new(&sbry_renderer, &drm_device.0, gbm_device, space);
        for o in kms.outputs() {
            o.create_global::<Sudbury>(display_handle);
        }

        #[cfg(any(feature = "tty-session", feature = "libseat"))]
        if let Err(e) =
            loop_handle.insert_source(notifier, move |event, &mut (), _data| match event {
                PauseSession => todo!(),
                ActivateSession => todo!(),
            })
        {
            error!("{e}");
            return None;
        }

        let Ok(token) = loop_handle.insert_source(drm_device.1, SudburyDrmKms::drm_event) else {
            error!("Failed to enable DRM event source");
            return None;
        };

        Some(Self {
            session,
            loop_handle: loop_handle.clone(),
            dev: sbry_renderer,
            kms,
            token,
            render_token: None,
            render_idle: RefCell::new(None),
        })
    }

    #[cfg(feature = "libseat")]
    pub fn session(&self) -> &LibSeatSession {
        &self.session
    }

    #[cfg(feature = "tty-session")]
    pub fn session(&self) -> &DirectSession {
        &self.session
    }

    #[cfg(not(any(feature = "tty-session", feature = "libseat")))]
    pub fn session(&self) -> &NullSession {
        &self.session
    }

    /// Returns the outputs of this [`SudburyDrmKms`].
    pub fn outputs(&self) -> Vec<Output> {
        self.kms.outputs()
    }

    /// Returns a mutable reference to the dmabuf state of this [`SudburyDrmKms`].
    pub fn dmabuf_state_mut(&mut self) -> &mut DmabufState {
        self.dev.dmabuf_state_mut()
    }

    /// Import a dmabuf as an EGL texture for use by the renderer.
    ///
    /// # Errors
    ///
    /// This function will return an error if .
    pub fn import_dmabuf(&mut self, dmabuf: &Dmabuf) -> anyhow::Result<()> {
        self.dev
            .renderer_mut()
            .import_dmabuf(dmabuf, None)
            .with_context(|| "Bad dmabuf import")?;
        Ok(())
    }

    pub fn set_input(&mut self, input: Rc<RefCell<SudburyInput>>) {
        input.borrow_mut().pointer.icon(self.dev.renderer_mut());
        self.dev.set_input(input);
    }

    #[cfg(feature = "fps")]
    pub fn fps(&self) -> (f64, f64, f64) {
        (
            self.kms.fps().avg(),
            self.kms.fps().min(),
            self.kms.fps().max(),
        )
    }

    /// Toggle whether or not the compositor will tint blitted frames
    pub fn tint(&mut self) {
        for compositor in self.kms.compositors_mut() {
            info!("Tinting");
            let mut flags = compositor.debug_flags();
            flags.toggle(DebugFlags::TINT);
            compositor.set_debug_flags(flags);
        }

        for output in self.kms.outputs() {
            self.immediate_composition(&output)
        }
    }
}
