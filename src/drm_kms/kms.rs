use std::collections::{HashMap, HashSet};

use anyhow::{bail, Context};
use smithay::{
    backend::{
        allocator::{gbm::GbmAllocator, Format},
        drm::{
            compositor::DrmCompositor, DrmDevice, DrmDeviceFd, DrmEventMetadata, DrmEventTime,
            DrmSurface,
        },
    },
    desktop::{utils::OutputPresentationFeedback, Space, Window},
    output::{self, Mode, Output, PhysicalProperties, Subpixel, WeakOutput},
    reexports::{
        drm::{
            self,
            control::{connector, crtc, property, Device, ResourceHandle},
        },
        gbm::{self, BufferObjectFlags},
        wayland_protocols::wp::presentation_time::server::wp_presentation_feedback,
    },
    utils::{Monotonic, Size, Time},
};
use tracing::{debug, error, trace, warn};

use crate::drm_kms::drm::SudburyDrmCompositor;

// The allocator and exporter should always match (GBM/dmabuf).
pub type InnerDrmCompositor = DrmCompositor<
    GbmAllocator<DrmDeviceFd>,
    gbm::Device<DrmDeviceFd>,
    Option<OutputPresentationFeedback>,
    DrmDeviceFd,
>;

/// Get a raw property value from KMS.
///
/// <https://www.kernel.org/doc/html/latest/gpu/drm-kms.html?highlight=properties#existing-kms-properties>
///
/// # Errors
///
/// This function will return an error if either the device properties, or the `name` cannot be
/// found.
fn get_property_val(
    device: &&(impl Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> anyhow::Result<(property::Handle, property::ValueType, property::RawValue)> {
    let props = device
        .get_properties(handle)
        .with_context(|| format!("Property error for {device:?}"))?;
    let (prop_handles, values) = props.as_props_and_values();
    for (&prop, &val) in prop_handles.iter().zip(values.iter()) {
        let info = device
            .get_property(prop)
            .with_context(|| format!("Property {name}"))?;
        if Some(name) == info.name().to_str().ok() {
            let val_type = info.value_type();
            return Ok((prop, val_type, val));
        }
    }
    anyhow::bail!("No prop found")
}

// Get a boolean property from KMS.
#[allow(clippy::just_underscores_and_digits)]
fn get_prop_bool(
    device: &(impl Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> bool {
    match get_property_val(&device, handle, name) {
        Ok((_0, _1, _2)) => match _1.convert_value(_2) {
            property::Value::Boolean(a) => a,
            _ => unreachable!(),
        },
        Err(e) => {
            error!("{e}");
            false
        }
    }
}

/// Get a blob property from KMS.
#[allow(clippy::just_underscores_and_digits)]
fn get_prop_blob(
    device: &(impl Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> Option<u64> {
    match get_property_val(&device, handle, name) {
        Ok((_0, _1, _2)) => match _1.convert_value(_2) {
            property::Value::Blob(a) => Some(a),
            _ => unreachable!(),
        },
        Err(e) => {
            error!("{e}");
            None
        }
    }
}

/// KMS related abstraction
#[derive(Debug)]
pub struct SudburyKms {
    #[allow(dead_code)]
    gbmdev: gbm::Device<DrmDeviceFd>,
    /// For each output, maps relationship of [`Output`] to a renderer/surface pair
    outputs: HashMap<WeakOutput, InnerDrmCompositor>,
    #[cfg(feature = "fps")]
    fps: fps_ticker::Fps,
}

impl SudburyKms {
    #[cfg(feature = "fps")]
    pub fn fps(&self) -> &fps_ticker::Fps {
        &self.fps
    }
}

struct KmsData {
    pub crtc: crtc::Handle,
}

impl KmsData {
    /// Returns the crtc of this [`KmsData`].
    ///
    /// This data is stored in the [`Output`] [`smithay::utils::user_data::UserDataMap`]
    pub fn crtc(&self) -> u32 {
        self.crtc.into()
    }
}

/// Get all possible CRTCs for a connector
fn crtcs(drm_device: &DrmDevice, connector: &connector::Info) -> HashSet<crtc::Handle> {
    HashSet::from_iter(
        connector
            .encoders()
            .iter()
            .flat_map(|eh| drm_device.get_encoder(*eh))
            .flat_map(|encoder_info| match drm_device.resource_handles() {
                Ok(handles) => handles.filter_crtcs(encoder_info.possible_crtcs()),
                Err(e) => {
                    error!("{e}");
                    vec![]
                }
            }),
    )
}

/// Get all connectors for the [`DrmDevice`]
fn connectors(drm_device: &DrmDevice) -> HashMap<connector::Handle, connector::Info> {
    let mut connectors: HashMap<connector::Handle, connector::Info> = HashMap::new();

    match drm_device.resource_handles() {
        Ok(rhandles) => rhandles.connectors().iter().for_each(|h| {
            if let Ok(info) = drm_device.get_connector(*h, false) {
                connectors.insert(*h, info);
            } else {
                debug!("Skipping connector handle {}", u32::from(*h));
            }
        }),
        Err(e) => {
            error!("{e}");
            return connectors;
        }
    };

    connectors
}

/// Returns the active mode for the connector if the connector is active
fn active_crtc_mode(drm_device: &DrmDevice, conn: &connector::Info) -> Option<Mode> {
    let eh = conn.current_encoder()?;
    let Ok(encoder) = drm_device.get_encoder(eh) else {
            return None;
    };
    let Ok(handles) = drm_device.resource_handles() else {
            return None;
    };
    let crtcs: Vec<crtc::Info> = handles
        .filter_crtcs(encoder.possible_crtcs())
        .iter()
        .filter(|&c| get_prop_bool(drm_device, *c, "ACTIVE"))
        .flat_map(|h| drm_device.get_crtc(*h).into_iter()) // This will silently drop any
        // failures.
        .collect();
    if crtcs.len() > 1 {
        warn!("Unexpected number of active CRTCS {crtcs:?}");
    }
    if !crtcs.is_empty() {
        let drm_mode = crtcs[0].mode()?;
        return Some(Mode {
            size: Size::from((drm_mode.size().0 as i32, drm_mode.size().1 as i32)),
            refresh: drm_mode.vrefresh() as i32,
        });
    }
    None
}

/// Returns name of manufacturer based on EDID spec.
///
/// <https://edid.tv/manufacturer/>
fn manufacturer(vendor: &[char; 3]) -> &'static str {
    match vendor {
        ['H', 'W', 'P'] => "Hewlett Packard",
        _ => "Unknown",
    }
}

/// Obtain EDID make and model for the connector
///
/// # Errors
///
/// This function will return an error if KMS EDID property couldn't be retrieved
fn make_model(drm_device: &DrmDevice, h: connector::Handle) -> anyhow::Result<(&str, &str)> {
    let edid_prop_blob = get_prop_blob(drm_device, h, "EDID").with_context(|| "")?;
    let blob = drm_device.get_property_blob(edid_prop_blob)?;
    // Little endian
    let vid = ((blob[1] as u16) << 8) | blob[0] as u16;
    let vendor: [char; 3] = [
        char::from_u32((vid & 0b0111110000000000 >> 10) as u32).unwrap_or_default(),
        char::from_u32((vid & 0b0000001111100000 >> 5) as u32).unwrap_or_default(),
        char::from_u32((vid & 0b0000000000011111) as u32).unwrap_or_default(),
    ];

    Ok((manufacturer(&vendor), "TODO"))
}

/// Get a name for the connector
fn connector_name(interface: connector::Interface, id: u32) -> String {
    let if_name = match interface {
        connector::Interface::DisplayPort => "DP".to_owned(),
        connector::Interface::HDMIA => "HDMI".to_owned(),
        connector::Interface::HDMIB => "HDMI".to_owned(),
        connector::Interface::EmbeddedDisplayPort => "eDP".to_owned(),
        connector::Interface::DSI => "DSI".to_owned(),
        _ => todo!(),
    };

    format!("{if_name}-{id}")
}

fn create_output(
    drm_device: &DrmDevice,
    connector: (&connector::Handle, &connector::Info),
) -> Output {
    let name = connector_name(connector.1.interface(), connector.1.interface_id());

    let (phys_w, phys_h) = connector.1.size().unwrap_or((0, 0));

    let (make, model) = make_model(drm_device, *connector.0).unwrap_or(("unknown", "unknown"));
    let output = Output::new(
        name,
        PhysicalProperties {
            size: (phys_w as i32, phys_h as i32).into(),
            subpixel: Subpixel::None, // Subpixel is old KMS found by the GETCONNECTOR ioctl.
            make: make.to_string(),
            model: model.to_string(),
        },
    );

    for mode in connector.1.modes() {
        let m = output::Mode {
            size: (mode.size().0 as i32, mode.size().1 as i32).into(),
            refresh: mode.vrefresh() as i32,
        };
        output.add_mode(m);
        if mode
            .mode_type()
            .contains(drm::control::ModeTypeFlags::PREFERRED)
        {
            output.set_preferred(m);
        }
    }

    output
}

fn find_crtc(
    drm_device: &DrmDevice,
    mode: Mode,
    connector: (&connector::Handle, &connector::Info),
) -> anyhow::Result<(crtc::Handle, DrmSurface)> {
    // Find a good CRTC for the added connector
    let all_crtcs = crtcs(drm_device, connector.1);
    let mut failed_crtcs: HashSet<crtc::Handle> = HashSet::new();
    for crtc in &all_crtcs {
        let current_refresh: u32 = u32::try_from(mode.refresh)?;
        if let Some(mode) = connector
            .1
            .modes()
            .iter()
            .find(|mode| mode.vrefresh() == current_refresh)
        {
            match drm_device.create_surface(*crtc, *mode, &[*connector.0]) {
                Ok(surf) => return Ok((*crtc, surf)), //surface.link(signaler);
                Err(e) => {
                    error!("{e}");
                    continue;
                }
            }
        } else {
            failed_crtcs.insert(*crtc);
        }
    }

    debug_assert_eq!(all_crtcs, failed_crtcs);
    bail!("No valid crtc found for connector {:?}", connector.0);
}

fn enumerate_outputs(
    drm_device: &DrmDevice,
    gbm_device: &gbm::Device<DrmDeviceFd>,
    space: &mut Space<Window>,
    formats: &HashSet<Format>,
) -> anyhow::Result<HashMap<WeakOutput, InnerDrmCompositor>> {
    let mut connected = connectors(drm_device);

    connected.retain(|_h, i| i.state() == connector::State::Connected);
    trace!(connectors = ?connected);

    let mut outputs = HashMap::with_capacity(2);
    for (connector_handle, connector_info) in connected {
        // Create an output for the connected device
        let output = create_output(drm_device, (&connector_handle, &connector_info));

        // If the crtc is active though, use that mode
        if let Some(active_mode) = active_crtc_mode(drm_device, &connector_info) {
            if output.modes().contains(&active_mode) {
                output.set_preferred(active_mode);
            } else {
                error!("Current mode {:?}, not in list of modes", active_mode);
            }
        }

        // This should only happen on inactive CRTCs.
        if output.preferred_mode() != output.current_mode() {
            output.change_current_state(output.preferred_mode(), None, None, None);
        }

        // If there are multiple outputs, stride it horizontally from left to right.
        let width = space.outputs().fold(0, |acc, out| {
            acc + out
                .current_mode()
                .unwrap_or(Mode {
                    size: (0, 0).into(),
                    refresh: 0,
                })
                .size
                .w
        });
        space.map_output(&output, (width, 0));
        output.change_current_state(None, None, None, Some((width, 0).into()));

        let scanout = match find_crtc(
            drm_device,
            output.current_mode().with_context(|| "Couldn't get mode")?,
            (&connector_handle, &connector_info),
        ) {
            Ok((crtc, surf)) => {
                output.user_data().insert_if_missing(|| KmsData { crtc });
                surf
            }
            Err(e) => {
                error!("{e}");
                continue;
            }
        };

        let planes = scanout.planes();

        let renderer = match DrmCompositor::new(
            &output,
            scanout,
            planes.ok(),
            GbmAllocator::new(
                gbm_device.clone(),
                BufferObjectFlags::RENDERING | BufferObjectFlags::SCANOUT,
            ),
            gbm_device.clone(),
            formats.to_owned(),
            drm_device.cursor_size(),
            Some(gbm_device.clone()),
        ) {
            Ok(r) => r,
            Err(e) => {
                error!("{e}");
                continue;
            }
        };

        outputs.insert(output.downgrade(), renderer);
    }

    Ok(outputs)
}

impl SudburyKms {
    pub(super) fn new(
        drm: &SudburyDrmCompositor,
        drmdev: &DrmDevice,
        gbmdev: gbm::Device<DrmDeviceFd>,
        space: &mut Space<Window>,
    ) -> Self {
        let outputs = match enumerate_outputs(drmdev, &gbmdev, space, drm.formats()) {
            Ok(o) => o,
            Err(_) => HashMap::new(),
        };
        Self {
            gbmdev,
            outputs,
            #[cfg(feature = "fps")]
            fps: fps_ticker::Fps::with_window_len(300),
        }
    }

    /// Initiates a swap for this [`SudburyKms`].
    ///
    /// # Errors
    ///
    /// This function will return an error if .
    pub fn swap(
        &mut self,
        output: &Output,
        feedback: OutputPresentationFeedback,
    ) -> anyhow::Result<()> {
        let scanout = self.scanout_data(output);
        scanout
            .queue_frame(Some(feedback))
            .with_context(|| "Swap fail")
    }

    /// Returns the outputs of this [`SudburyKms`].
    pub(super) fn outputs(&self) -> Vec<Output> {
        let mut x: Vec<Output> = self
            .outputs
            .iter()
            .filter_map(|(o, _)| o.upgrade())
            .collect();
        x.sort_by(|a, b| {
            a.user_data()
                .get::<KmsData>()
                .map(|kmsdata| kmsdata.crtc())
                .cmp(&b.user_data().get::<KmsData>().map(|kmsdata| kmsdata.crtc()))
        });
        x
    }

    /// Returns the compositors of this [`SudburyKms`].
    pub fn compositors_mut(&mut self) -> Vec<&mut InnerDrmCompositor> {
        self.outputs.values_mut().collect()
    }

    #[allow(dead_code)]
    fn get_output(&self, crtc: crtc::Handle) -> Option<Output> {
        self.outputs
            .iter()
            .filter_map(|(o, _)| o.upgrade())
            .find(|y| {
                y.user_data()
                    .get::<KmsData>()
                    .filter(|kmsdata| kmsdata.crtc() == u32::from(crtc))
                    .is_some()
            })
    }

    pub(super) fn scanout_data(&mut self, output: &Output) -> &mut InnerDrmCompositor {
        self.outputs.get_mut(&output.downgrade()).unwrap()
    }

    fn present(
        output: &Output,
        feedback: Option<OutputPresentationFeedback>,
        flip_info: &DrmEventMetadata,
    ) {
        let Some(mut presentation) = feedback else { error!("Presentation Feedback missing"); return };

        let (c, f) = if let DrmEventTime::Monotonic(m) = flip_info.time {
            (
                Time::<Monotonic>::from(m),
                wp_presentation_feedback::Kind::Vsync
                    | wp_presentation_feedback::Kind::HwClock
                    | wp_presentation_feedback::Kind::HwCompletion,
            )
        } else {
            crate::MONO_CLOCK.with(|clock| (clock.now(), wp_presentation_feedback::Kind::Vsync))
        };

        presentation.presented(
            c,
            output
                .current_mode()
                .map(|mode| mode.refresh as u32)
                .unwrap_or_default(),
            flip_info.sequence as u64,
            f,
        );
    }

    pub(super) fn vblank(&mut self, crtc: crtc::Handle, meta: &DrmEventMetadata) -> Option<Output> {
        #[cfg(feature = "fps")]
        self.fps.tick();
        if let Some(mut output) = self.outputs.iter_mut().find(|(weakout, _)| {
            weakout
                .upgrade()
                .and_then(|output| {
                    output
                        .user_data()
                        .get::<KmsData>()
                        .map(|kdata| kdata.crtc() == crtc.into())
                })
                .is_some()
        }) {
            let gbm = &mut output.1;
            match gbm.frame_submitted() {
                Ok(pres) => {
                    if let Some(out) = output.0.upgrade() {
                        let pres = pres.flatten();
                        Self::present(&out, pres, meta);
                        return Some(out);
                    }
                }
                Err(e) => {
                    error!("Frame submission: {e} (missed frame_queue)");
                    return None;
                }
            };
        } else {
            warn!("Couldn't find CRTC to handle vblank")
        }
        None
    }
}
