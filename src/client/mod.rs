//! Handles Wayland Client specifics.

use smithay::reexports::wayland_server;
use tracing::{instrument, trace};

#[derive(Debug)]
pub struct WaylandClient;

impl wayland_server::backend::ClientData for WaylandClient {
    fn initialized(&self, client_id: wayland_server::backend::ClientId) {
        trace!("Initialized {client_id:?}");
    }
    #[instrument(level = "debug")]
    fn disconnected(
        &self,
        client_id: wayland_server::backend::ClientId,
        reason: wayland_server::backend::DisconnectReason,
    ) {
        trace!("{client_id:?} {reason:?}");
    }
}
