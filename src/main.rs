//! Sudbury compositor for Chrome OS

#[cfg(feature = "fps")]
use std::time::Duration;
use std::{
    cell::RefCell,
    fs::File,
    io::stdout,
    os::unix::{net::UnixStream, prelude::AsRawFd},
    path::PathBuf,
    rc::Rc,
    sync::Arc,
};

use anyhow::{Context, Result};
use gumdrop::Options;
use smithay::{
    desktop::{PopupManager, Space, Window},
    input::{Seat, SeatState},
    reexports::{
        calloop::{
            self, generic::Generic, EventLoop, Interest, LoopHandle, LoopSignal, PostAction,
            Readiness,
        },
        wayland_server::{Display, DisplayHandle},
    },
    utils::{Clock, Monotonic},
    wayland::{
        compositor::CompositorState, data_device::DataDeviceState,
        input_method::InputMethodManagerState, output::OutputManagerState,
        presentation::PresentationState, primary_selection::PrimarySelectionState,
        relative_pointer::RelativePointerManagerState, shell::xdg::XdgShellState, shm::ShmState,
        socket::ListeningSocketSource, text_input::TextInputManagerState,
        viewporter::ViewporterState, virtual_keyboard::VirtualKeyboardManagerState,
    },
};
use std::fmt::Debug;
use tracing::{debug, debug_span, error, info, trace};
use tracing_appender::non_blocking;
use tracing_subscriber::filter::EnvFilter;

mod core_interfaces;
mod external_interfaces;
mod stable_interfaces;
mod staging_interfaces;
mod unstable_interfaces;

mod client;
mod drm_kms;
mod input;
mod session;

use input::SudburyInput;

use drm_kms::SudburyDrmKms;

use crate::{client::WaylandClient, external_interfaces::croscomp::CroscompState};

thread_local!(static MONO_CLOCK: Clock<Monotonic> = Clock::new().expect("Couldn't initialize clock"));

/// Default clear color for new surfaces.
pub const CLEAR_COLOR: [f32; 4] = [0.0, 0.7, 0.3, 1.0];
pub const FULLSCREEN_CLEAR_COLOR: [f32; 4] = [0.0, 0.0, 0.0, 0.0];

/// Static global state for Sudbury, the binary.
///
/// Calloop state is the true top level state of this application. All events are dispatched via this
/// structure and the connection to the main [`Display`] cannot be severed.
#[derive(Debug)]
pub struct GlobalState {
    display: Display<Sudbury>,
    sudbury: Sudbury,
}

/// Static global state for the compositor.
#[derive(Debug)]
pub struct Sudbury {
    /// A handle to the global display_handle
    ///
    /// Since [`GlobalState`] owns the actual [`Display`], the handle is kept for all display
    /// related things. Display in this context means the [Wayland global singleton](https://wayland.freedesktop.org/docs/html/apa.html#protocol-spec-wl_display).
    /// This display handle is cheaply clonable.
    #[allow(dead_code)]
    display_handle: DisplayHandle,

    /// Handle do the main Calloop
    /// [LoopHandle](https://docs.rs/calloop/latest/calloop/struct.LoopHandle.html). The eventloop
    /// handle is cheaply clonable.
    eventloop_handle: (LoopHandle<'static, GlobalState>, LoopSignal),

    /// The Wayland socket being used by the compositor.
    socket: String,

    /// Represents a Chrome [desk](https://support.google.com/chromebook/answer/9594869)
    ///
    /// A [`Space`] is an abstract concept representing just a 2d space of pixels.
    ///
    /// Some examples:
    /// 1: provide a traditional desktop with no extra workspaces
    /// N: provide a traditional desktop where each space is its own workspace.
    /// MxN: provide a tiled desktop with M windows/tiles over N workspaces
    ///
    /// Sudbury opts for the middle example from above.
    spaces: Vec<Space<Window>>,

    /// Popup manager
    popups: PopupManager,

    /// Surfaces not yet mapped to a space.
    ///
    /// In Wayland/XDG, surfaces are created (and committed) before a buffer is attached. It is not
    /// useful to map the window to an [`Output`](smithay::output::Output) until a buffer is
    /// attached. Orphan windows are those which have a buffer attached but haven't yet been
    /// assigned to an output.
    ///
    /// Specifically, these are
    /// [XDG Toplevels](https://wayland.app/protocols/xdg-shell#xdg_toplevel)
    orphan_windows: Vec<Window>,

    /// Sudbury only ever has one [`Seat`]. This is it.
    ///
    /// The seat arbitrates access over client focus and is used to obtain handles to the keyboard
    /// and mouse pointer.
    seat: Seat<Sudbury>,
    seat_state: SeatState<Sudbury>,

    /// Rendering, modesetting, and other related functionality
    ///
    /// Currently DRM/KMS only supports Gles2/Egl rendering.
    drm_kms: SudburyDrmKms,

    /// The entity that handles inputs
    input: Option<Rc<RefCell<SudburyInput>>>,

    /// Everything needs Shm
    shm_state: ShmState,

    /// Mandatory compositor state
    ///
    /// See [core_interfaces::compositor]
    comp_state: CompositorState,

    /// Utilize the XdgShell protocol
    ///
    /// See [stable_interfaces::xdg_shell]
    xdg_state: XdgShellState,

    ///
    ///
    ///
    data_device_state: DataDeviceState,

    /// Utilize the Primary Selection protocol
    ///
    /// https://wayland.app/protocols/primary-selection-unstable-v1
    primary_selection_state: PrimarySelectionState,
}

impl Sudbury {
    /// Initialize the Wayland socket
    ///
    /// # Errors
    ///
    /// This function will return an error if socket creation fails.

    fn init_socket(path: Option<&str>) -> anyhow::Result<(ListeningSocketSource, String)> {
        let source = match path {
            Some(p) => ListeningSocketSource::with_name(p)?,
            None => ListeningSocketSource::new_auto()?,
        };

        let name = source.socket_name().to_string_lossy().into_owned();

        debug!(socket = name);

        Ok((source, name))
    }

    /// Returns the current space number of this [`Sudbury`].
    pub fn current_space_no(&self) -> usize {
        *self.seat.user_data().get::<usize>().unwrap_or(&0)
    }

    /// Returns a reference to the current space of this [`Sudbury`].
    ///
    /// Operation is technically infallible but in case of issue, the default (0th) space will be
    /// returned. Sudbury policy is such that only one space can be active at a time.
    pub fn current_space(&self) -> &Space<Window> {
        let no = *self.seat.user_data().get::<usize>().unwrap_or(&0);
        &self.spaces[no]
    }

    pub fn current_space_mut(&mut self) -> &mut Space<Window> {
        let no = *self.seat.user_data().get::<usize>().unwrap_or(&0);
        &mut self.spaces[no]
    }

    /// Create a new Sudbury compostior.
    ///
    /// Returns None if critical initialization fails.
    pub fn new(
        display: (DisplayHandle, i32),
        eventloop: (LoopHandle<'static, GlobalState>, LoopSignal),
        path: PathBuf,
    ) -> Option<Self> {
        // TODO: Should socket creation happen last?
        let (sock, socket) = match Self::init_socket(None) {
            Ok((so, sok)) => (so, sok),
            Err(e) => {
                error!("{e}");
                return None;
            }
        };

        let comp_state = CompositorState::new::<Self>(&display.0);
        let _croscomp = CroscompState::new(&display.0);
        let data_device_state = DataDeviceState::new::<Self>(&display.0);
        InputMethodManagerState::new::<Self>(&display.0);
        let _output_manager_state = OutputManagerState::new_with_xdg_output::<Self>(&display.0);
        if let Ok(clock) = Clock::<Monotonic>::new() {
            let _pres_feed = PresentationState::new::<Self>(&display.0, clock.id() as u32);
        }
        let primary_selection_state = PrimarySelectionState::new::<Self>(&display.0);
        let mut seat_state = SeatState::new();
        let shm_state = ShmState::new::<Self>(&display.0, vec![]);
        RelativePointerManagerState::new::<Self>(&display.0);
        TextInputManagerState::new::<Self>(&display.0);
        let _vp_state = ViewporterState::new::<Self>(&display.0);
        VirtualKeyboardManagerState::new::<Self, _>(&display.0, |_client| true);
        let xdg_state = XdgShellState::new::<Self>(&display.0);

        let seat = seat_state.new_wl_seat(&display.0, "big nickel");

        // Set the current space for the seat.
        assert!(seat.user_data().insert_if_missing(|| 0_usize));

        // Add wayland socket connection events to the event loop
        if let Err(e) = eventloop
            .0
            .insert_source(sock, Self::handle_socket_connection)
        {
            error!("{e}");
            return None;
        }

        // Add a new event handlers for the Wayland global display
        if let Err(e) = eventloop.0.insert_source(
            Generic::new(display.1, Interest::READ, calloop::Mode::Level),
            Self::handle_client_request,
        ) {
            error!("{e}");
            return None;
        }

        // Always starts up with one space, for now.
        let mut first_space = Space::default();

        let drm_kms = SudburyDrmKms::new(&display.0, &eventloop.0, path, &mut first_space)?;

        Some(Self {
            eventloop_handle: eventloop,
            display_handle: display.0,
            socket,

            spaces: vec![first_space],
            popups: PopupManager::default(),
            orphan_windows: Vec::new(),

            seat,
            drm_kms,
            input: None,

            shm_state,
            comp_state,
            xdg_state,
            data_device_state,
            seat_state,
            primary_selection_state,
        })
    }

    /// Callback for client requests
    ///
    /// Dispatches all requests via the global display object. This is essentially a passthrough
    /// function to initiate actual work done by Smithay.
    ///
    /// # Errors
    ///
    /// This function will return an error if dispatch_client fails.
    pub fn handle_client_request(
        _event: Readiness,
        _meta: &mut i32,
        data: &mut GlobalState,
    ) -> Result<PostAction, std::io::Error> {
        let sudbury = &mut data.sudbury;
        let display = &mut data.display;

        debug_span!("client_dispatch");
        trace!("Dispatching client requests");

        // dispatch_clients() will block if there are no messages
        display.dispatch_clients(sudbury)?;

        Ok(PostAction::Continue)
    }

    /// Callback for socket connections
    ///
    /// Wayland protocol is all socket based. The only use of this function is to add the new
    /// Wayland client to the Smithay Display
    pub fn handle_socket_connection(event: UnixStream, _meta: &mut (), data: &mut GlobalState) {
        let display = &data.display;

        trace!("Socket connection");
        // Add the new client to the display server
        if let Err(e) = display
            .handle()
            .insert_client(event, Arc::new(WaylandClient))
        {
            error!("Failed to add new client {e}");
        }
    }

    /// Returns a reference to the eventloop handle of this [`Sudbury`].
    pub fn eventloop_handle(&self) -> &LoopHandle<'static, GlobalState> {
        &self.eventloop_handle.0
    }
}

/// Sudbury Display server
///
/// Wayland server for use in ChromeOS.
#[derive(Debug, Options)]
struct Args {
    #[options(help = "print help message")]
    help: bool,

    #[options(
        help = "File for sudbury's tracing output",
        meta = "/path/to/sudbury.log"
    )]
    sudbury_log: Option<PathBuf>,

    #[options(
        help = "Name of the DRM device node",
        meta = "cardX",
        default = "card0"
    )]
    drm_dev: String,

    #[options(help = "Print FPS statistics")]
    fps: bool,
}

fn to_node(x: Option<String>) -> PathBuf {
    let mut path = PathBuf::from("/dev/dri");
    if let Some(x) = x {
        path.push(x);
        path
    } else {
        todo!();
    }
}

#[allow(unused)]
fn fps_display(handle: LoopHandle<'static, GlobalState>) {
    #[cfg(feature = "fps")]
    {
        let fps_display = calloop::timer::Timer::from_duration(Duration::from_secs(5));
        handle
            .insert_source(fps_display, |_, _, data| {
                println!("FPS: {:?}", data.sudbury.drm_kms.fps(),);
                calloop::timer::TimeoutAction::ToDuration(Duration::from_secs(5))
            })
            .map_err(|e| error!("Failed to start FPS ticker {e}"))
            .ok();
    }
}

fn main() -> Result<()> {
    let args = Args::parse_args_default_or_exit();

    #[cfg(feature = "stack-overflow-bt")]
    unsafe {
        backtrace_on_stack_overflow::enable()
    };

    // Guard is needed to make sure final logs are flushed. Don't set this to _.
    let (non_blocking, _guard) = if let Some(sud_log) = args.sudbury_log {
        let sud_log = File::create(sud_log)?;
        non_blocking(sud_log)
    } else {
        non_blocking(stdout())
    };

    tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .with_writer(non_blocking)
        .init();

    // The display represents the single global wayland display object ID 1.
    let mut display: Display<Sudbury> = Display::new().with_context(|| "Failed to init display")?;

    let mut event_loop: EventLoop<GlobalState> =
        EventLoop::try_new().with_context(|| "Failed to initialize event loop")?;

    let signal = event_loop.get_signal();

    let mut sudbury = Sudbury::new(
        (display.handle(), display.backend().poll_fd().as_raw_fd()),
        (event_loop.handle(), signal.clone()),
        to_node(Some(args.drm_dev)),
    )
    .with_context(|| "Couldn't create display server")?;

    let input = SudburyInput::new(&mut sudbury);
    if let Some(input) = input {
        let inputrc = Rc::new(RefCell::new(input));
        sudbury.input = Some(Rc::clone(&inputrc));
        sudbury.drm_kms.set_input(Rc::clone(&inputrc));
    }
    if sudbury.input.is_none() {
        info!("No input system available");
    }

    std::env::set_var("WAYLAND_DISPLAY", &sudbury.socket);
    info!("Advertising Wayland socket {}", sudbury.socket);

    // Handle ctrl+c
    ctrlc::set_handler(move || {
        info!("ctrl+c received. Stopping...");
        signal.stop();
        signal.wakeup();
    })
    .expect("Error setting Ctrl-C handler");

    // Package up the callback data. EventLoop now owns Sudbury
    let mut event_data = GlobalState { display, sudbury };

    if args.fps {
        fps_display(event_loop.handle())
    }

    debug!("Starting event loop");
    // With no timeout, the event loop should only do something if a dispatch of any sort has occurred.
    // After any dispatching, it's important to flush buffers back to the client.
    event_loop.run(None, &mut event_data, |data| {
        if let Err(e) = data.display.flush_clients() {
            error!("Failed to flush buffers to clients {e}");
        }
    })?;
    debug!("Ending event loop");

    std::mem::drop(event_loop);

    Ok(())
}
