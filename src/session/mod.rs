// See https://github.com/rust-lang/cargo/issues/2980
#[cfg(all(feature = "tty-session", feature = "libseat"))]
compile_error!("Can't use both tty session and libseat session");

#[cfg(not(any(feature = "tty-session", feature = "libseat")))]
pub mod null;

#[cfg(feature = "tty-session")]
pub mod direct;
