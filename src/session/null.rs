use std::{
    collections::HashMap,
    os::fd::RawFd,
    path::{Path, PathBuf},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use smithay::{
    backend::session::{AsErrno, Session},
    reexports::nix,
};
use tracing::{debug, error};

/// [`Session`] via the lack of session
#[derive(Clone, Debug)]
pub struct NullSession {
    active: Arc<AtomicBool>,
    devices: HashMap<PathBuf, RawFd>,
}

impl Drop for NullSession {
    fn drop(&mut self) {
        debug!("Null session dropped");
    }
}

impl Session for NullSession {
    type Error = Error;

    fn open(&mut self, path: &Path, flags: nix::fcntl::OFlag) -> Result<RawFd, Self::Error> {
        debug!("Opening device {path:?}");
        if self.devices.get(path).is_some() {
            return Err(Error::Busy(path.to_string_lossy().into()));
        }
        let fd = nix::fcntl::open(path, flags, nix::sys::stat::Mode::empty())
            .map_err(|errno| Error::FailedToOpen(path.to_string_lossy().into(), errno))?;
        self.devices.insert(path.to_path_buf(), fd);
        Ok(fd)
    }

    fn close(&mut self, fd: RawFd) -> Result<(), Self::Error> {
        self.devices.retain(|_, v| v != &fd);
        if let Err(e) = nix::unistd::close(fd) {
            error!("Failed to close : {e}");
        }
        Ok(())
    }

    fn change_vt(&mut self, _vt: i32) -> Result<(), Self::Error> {
        unimplemented!("VTs don't exist in Chrome")
    }

    fn is_active(&self) -> bool {
        self.active.load(Ordering::SeqCst)
    }

    fn seat(&self) -> String {
        // Only ever one seat
        String::from("seat0")
    }
}

impl NullSession {
    pub fn new() -> NullSession {
        NullSession {
            active: Arc::new(AtomicBool::new(true)),
            devices: HashMap::new(),
        }
    }
}

impl AsErrno for Error {
    fn as_errno(&self) -> Option<i32> {
        match self {
            Error::Busy(_) => todo!(),
            Error::FailedToOpen(_, _) => todo!(),
        }
    }
}

/// Errors related to Null sessions
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// Failed to open TTY
    #[error("One file per session ")]
    Busy(String),
    /// Failed to open TTY
    #[error("Failed to open TTY `{0}`")]
    FailedToOpen(String, #[source] nix::Error),
}
